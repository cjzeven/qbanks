<?php if( ! isset($_COOKIE['token']) || empty($_COOKIE['token'])) { exit('You dont have permission!'); } ?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <title>QBanks - @yield('title')</title>
   <link rel="stylesheet" type="text/css" href="../assets/bootstrap/css/bootstrap.css">
   <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
   <link rel="stylesheet" type="text/css" href="../assets/js/datatables/css/dataTables.bootstrap.min.css">     
   <link rel="stylesheet" type="text/css" href="../assets/js/jqueryui/jquery-ui.min.css">
</head>
<body ng-app="qbanksApp">

<div class="content">
 <div class="container">
    <div class="row">
       <div class="col-md-12">
          <div class="page-header">
             <div class="page-header-dashboard">
                <h1>@yield('menu_title')</h1>
                <h4 class="text-right pull-right total_balance"><small>Balance Total:</small> IDR <span>0</span></h4>
             </div>
          </div>
          <!-- Notification -->
          <div id="notif" class="alert notification" role="alert">
             <ul>
             </ul>
          </div>
          <div class="row">
             <!-- Sidebar menu -->
            @yield('menubar')

            @yield('content')
          </div>
       </div>
    </div>
 </div>
 </div>

 <div ng-controller="authorizeController"></div>

</body>
<script type="text/javascript" src="../assets/js/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="../assets/js/datatables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../assets/js/datatables/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="../assets/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="../assets/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="../assets/js/script.js"></script>
<script type="text/javascript" src="../assets/js/moment.min.js"></script>
<script type="text/javascript" src="../assets/js/angular.min.js"></script>
<script type="text/javascript" src="../assets/js/angular-script.js"></script>
<script type="text/javascript" src="../assets/js/jqueryui/jquery-ui.min.js"></script>
</html>