@yield('authorized')

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>QBanks - @yield('title')</title>
	<link rel="stylesheet" type="text/css" href="../assets/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
</head>
<body ng-app="qbanksApp">

@yield('content')

</body>
<script type="text/javascript" src="../assets/js/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="../assets/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="../assets/js/angular.min.js"></script>
<script type="text/javascript" src="../assets/js/angular-script.js"></script>
</html>