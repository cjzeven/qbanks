@extends('master.master')

@section('title', 'Forgot Password')

@section('content')

<div class="content" ng-controller="forgotController">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="page-header">
					  <h1>Forgot Password</h1>
					</div>

					<!-- Notification -->		
					<div class="alert alert-danger notif" role="alert">
					  <ul>
					  	<li ng-repeat="error in errorData"><## error.message ##></li>
					  </ul>				  
					</div>	

					<div class="alert alert-success notif" role="alert">
						<span>Token sent, please check your email!</span>				  
					</div>													


					<form id="form_forgot" class="form-horizontal" novalidate>
					  <div class="form-group">
					    <label for="email" class="col-sm-2 control-label">Email</label>
					    <div class="col-sm-10">
					      <input name="email" type="email" class="form-control" id="email" placeholder="Email" ng-model="email">
					    </div>
					  </div>
					  <div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10">
					      <button id="forgot" type="submit" class="btn btn-primary" ng-click="forgot()">Send Reset Token</button>
					      <a class="btn btn-danger pull-right" type="button" href="/">Cancel</a>
					    </div>
					  </div>
					</form>				
				</div>
			</div>
		</div>		
	</div>
</div>
@endsection