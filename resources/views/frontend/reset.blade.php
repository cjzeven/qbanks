@extends('master.master')

@section('title', 'Reset Password')

@section('content')

<div class="content" ng-controller="resetController">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="page-header">
					  <h1>Reset Password</h1>
					</div>

					<!-- Notification -->		
					<div class="alert alert-danger notif" role="alert">
					  <ul>
					  	<li ng-repeat="error in errorData"><## error.message ##></li>
					  </ul>				  
					</div>	

					<div class="alert alert-success notif" role="alert">
						<span>Reset success, redirecting...</span>				  
					</div>													


					<form id="form_reset">
					  <div class="form-group">
					    <label for="token" class="control-label">Token</label>
					    <input name="token" type="text" class="form-control" id="token" placeholder="Token" ng-model="token">
					  </div>
					  <div class="form-group">
					    <label for="new_password" class="control-label">New Password</label>
					    <input name="new_password" type="password" class="form-control" id="password" placeholder="Password" ng-model="password">
					  </div>	
					  <div class="form-group">
					    <label for="confirm_password" class="control-label">Confirm New Password</label>
					    <input name="new_password_confirm" type="password" class="form-control" id="confirm_password" placeholder="Confirm password" ng-model="confirmPassword">
					  </div>						  					  					  				  
					  <button id="reset" type="submit" class="btn btn-primary" ng-click="reset()">Reset</button>
					  <a class="btn btn-danger pull-right" type="button" href="/">Cancel</a>
					</form>			
				</div>
			</div>
		</div>		
	</div>
</div>
@endsection