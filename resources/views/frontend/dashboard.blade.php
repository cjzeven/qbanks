@extends('master.master_member')
@extends('frontend.menu')

@section('title', 'Transaction History')
@section('menu_title', 'History')
@section('history_active', 'active')

@section('content')

 <div class="col-md-9 loading-area" ng-controller="dashboardController">
      <!-- Notification -->   
      <div class="alert alert-danger notif" role="alert">
        <ul><li><## errorData.message ##></li></ul>         
      </div>   
      <div>
       <!-- Nav tabs -->
       <!-- Bank List -->
       <ul class="nav nav-tabs" role="tablist">
            <li ng-repeat="bankList in bankLists" role="presentation" class="<## bankList.active ##>">
               <a href=" #<##bankList.accountNo##>" aria-controls="home" role="tab" data-toggle="tab"><b><## bankList.name  ##> (<## bankList.accountNo ##>)</b></a>
            </li>
       </ul>
       <!-- Tab panes -->
       <div class="tab-content" id="tab_history">
         <!-- <div ng-repeat="bankList in bankLists"> -->
            <div ng-repeat="bankList in bankLists" role="tabpanel" class="tab-pane <## bankList.active ##>" id="<## bankList.accountNo ##>">
               <br>
               <ul class="list-group">
                  <!-- Start Search Form -->
                  <div class="panel panel-default">
                     <div class="panel-body">
                         <form class="form-inline">
                            <div class="form-group">
                               <!-- <label for="exampleInputName2">Update by Date: </label>  -->
                               <div class="input-group">
                                  <select ng-model="mySearchByItem" ng-options="item.label for item in searchByItems track by item.id" class="form-control"></select>
                               </div>
                               <div class="input-group"> 
                                 <input placeholder="Pick start date" type="text" class="form-control" ng-model="dateFrom" id="date_from_<## bankList.accountNo ##>"> 
                                 <span class="input-group-addon">to</span> 
                                 <input placeholder="Pick end date"  type="text" class="form-control" ng-model="dateTo" id="date_to_<## bankList.accountNo ##>">
                                 <input type="hidden" class="form-control" ng-model="bankList.secretKey">                                                
                                 <input type="hidden" class="form-control" ng-model="bankList.accountNo">                                                
                               </div>
                            </div>
                  <button type="button" class="btn btn-primary" ng-click="search(dateFrom, dateTo, bankList.secretKey, bankList.bankAccountId, mySearchByItem.id)">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span> Search
                  </button>                              
                  <button type="button" class="btn btn-danger pull-right" ng-click="refresh()">
                    <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Refresh
                  </button>         
                         </form>                                     
                     </div>
                  </div>
                  <!-- End Search Form -->

                  <!-- Start Table Content -->

                  <!-- End Table Content -->
               </ul>
            </div>                           
       </div>
    </div>
 </div>

@endsection