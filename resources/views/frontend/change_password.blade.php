@extends('master.master_member')
@extends('frontend.menu')

@section('title', 'Change Password')
@section('menu_title', 'Change Password')
@section('settings_active', 'active')

@section('content')

<div class="col-md-9 loading-area" ng-controller="changePasswordController">
  <div>
	<!-- Notification -->		
	<div class="alert alert-danger notif" role="alert">
	  <ul>
	  	<li ng-repeat="error in errorData"><## error.message ##></li>
	  </ul>				  
	</div>	

	<div class="alert alert-success notif" role="alert">
		<span>Password changed, reload...</span>				  
	</div>	

	<form id="form_change_password">
	  <div class="form-group">
	    <label for="password" class="control-label">Password</label>
	      <input name="password" type="password" class="form-control" id="password" placeholder="Password" ng-model="password">
	  </div>
	  <div class="form-group">
	    <label for="new_password" class="control-label">New Password</label>
	      <input name="new_password" type="password" class="form-control" id="new_password" placeholder="New Password" ng-model="new_password">
	  </div>	
	  <div class="form-group">
	    <label for="confirm_new_password" class="control-label">Confirm New Password</label>
	      <input name="new_password_confirm" type="password" class="form-control" id="confirm_new_password" placeholder="Confirm New Password" ng-model="confirm_new_password">
	  </div>
	  <button id="change" type="submit" class="btn btn-primary" ng-click="changePassword(password, new_password, confirm_new_password)">Change</button>
	</form>			
  </div>
</div>

@endsection