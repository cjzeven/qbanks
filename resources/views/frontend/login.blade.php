@extends('master.master')

@section('title', 'Login')

@section('content')

<div class="content" ng-controller="loginController">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="page-header">
					  <h1>Member Login</h1>
					</div>

					<!-- Notification -->		
					<div class="alert alert-danger notif" role="alert">
					  <ul>
					  	<li ng-repeat="error in errorData"><## error.message ##></li>
					  </ul>				  
					</div>	

					<div class="alert alert-success notif" role="alert">
						<span>Login success, redirecting...</span>				  
					</div>																		

					<form id="form_login" class="form-horizontal">
					  <div class="form-group">
					    <label for="username" class="col-sm-2 control-label">Username</label>
					    <div class="col-sm-10">
					      <input name="username" type="text" class="form-control" id="username" placeholder="Username" ng-model="username">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="password" class="col-sm-2 control-label">Password</label>
					    <div class="col-sm-10">
					      <input name="password" type="password" class="form-control" id="password" placeholder="Password" ng-model="password">
					    </div>
					  </div>
					  <div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10">
					      <input id="login" type="submit" class="btn btn-primary" ng-click="login()" value="Login">
					     <a class="pull-right" href="forgot">Forgot</a>
					     <a class="pull-right">&nbsp;|&nbsp;</a>
					     <a class="pull-right" href="register">Register Me!</a>
					    </div>
					  </div>
					</form>				
				</div>
			</div>
		</div>		
	</div>
</div>
@endsection