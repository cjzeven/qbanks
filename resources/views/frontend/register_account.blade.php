@extends('master.master_member')
@extends('frontend.menu')

@section('title', 'Register Bank Account')
@section('menu_title', 'Register Bank')
@section('register_account_active', 'active')

@section('content')

<div class="col-md-9 loading-area" ng-controller="registerBankController">
  <div>
	<!-- Notification -->		
	<div class="alert alert-danger notif" role="alert">
	  <ul>
	  	<li ng-repeat="error in errorData"><## error.message ##></li>
	  </ul>				  
	</div>	

	<div class="alert alert-success notif" role="alert">
		<span>Register bank success, reload...</span>				  
	</div>	

	<form id="form_register_account">
	  <div class="form-group">
	    <label for="bank" class="control-label">Choose Bank</label>
			<select name="bank_id" class="form-control" ng-model="bank" ng-options="bankList.name for bankList in bankLists track by bankList.id">
			</select>
	  </div>
	  <div class="form-group">
	    <label for="account" class="control-label">Account Number</label>
	      <input name="account_number" type="text" class="form-control" id="account" placeholder="Account number" ng-model="accountNo">
	  </div>
	  <div class="form-group">
	    <label for="username" class="control-label">Username</label>
	      <input name="username" type="text" class="form-control" id="username" placeholder="Username" ng-model="username">
	  </div>	
	  <div class="form-group">
	    <label for="password" class="control-label">Password</label>
	      <input name="password" type="password" class="form-control" id="password" placeholder="Password" ng-model="password">
	  </div>
	  <button id="add" type="submit" class="btn btn-primary" ng-click="registerAccount(bank.id, username, password, accountNo)">Add Account</button>
	</form>			
  </div>
</div>
@endsection