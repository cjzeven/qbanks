@extends('master.master_member')
@extends('frontend.menu')

@section('title', 'Logout')
@section('logout_active', 'active')
@section('menu_title', 'Logout')

@section('content')
<div ng-controller="logoutController"></div>
@endsection