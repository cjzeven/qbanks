@extends('master.master_member')
@extends('frontend.menu')

@section('title', 'Balance History')
@section('menu_title', 'Balance')
@section('balance_active', 'active')

@section('content')

<div class="col-md-9 loading-area" ng-controller="balanceController">
  <div>
    <!-- Nav tabs -->
    <!-- Bank List -->
    <ul class="nav nav-tabs" role="tablist">
      <li ng-repeat="bankList in bankLists" role="presentation" class="<## bankList.active ##>">
        <a href=" #<##bankList.accountNo##>" aria-controls="home" role="tab" data-toggle="tab"><b><## bankList.name  ##> (<## bankList.accountNo ##>)</b></a>
      </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content" id="tab_history">
      <!-- <div ng-repeat="bankList in bankLists"> -->
      <div ng-repeat="bankList in bankLists" role="tabpanel" class="tab-pane <## bankList.active ##>" id="<## bankList.accountNo ##>">
        <br>
        <div ng-init="getBalance(bankList.secretKey, bankList.bankAccountId)" class="alert alert-danger active_balance" role="alert">Your balance: <strong>IDR <span>0</span></strong></div>
        <!-- Start Table Content -->
        <span ng-init="getBalanceHistory(bankList.bankAccountId)"></span>
        <!-- End Table Content -->
        </ul>
      </div>
    </div>
  </div>
</div>

@endsection