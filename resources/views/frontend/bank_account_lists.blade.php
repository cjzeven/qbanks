@extends('master.master_member')
@extends('frontend.menu')

@section('title', 'Bank Account Lists')
@section('menu_title', 'Bank Account Lists')
@section('settings_active', 'active')

@section('content')

<div class="col-md-9 loading-area" ng-controller="removeAccountController">
  <div>
	<!-- Notification -->		
	<div class="alert alert-danger notif" role="alert">
	  <ul>
	  	<li ng-repeat="error in errorData"><## error.message ##></li>
	  </ul>				  
	</div>	

	<div class="alert alert-success notif" role="alert">
		<span>Bank account deleted, reload...</span>				  
	</div>	  
	<table class="table table-hover table-bordered">
		<thead>
			<th>No</th>
			<th>Account Number</th>
			<th>Vendor</th>
			<th>Created At</th>
			<th>Actions</th>
		</thead>
		<tbody>
			<tr ng-repeat="account in accounts">
				<td><## ($index + 1) ##></td>
				<td><## account.account_no ##></td>
				<td><## account.bank_accounts.name ##></td>
				<td><## (account.bank_accounts.created_at).split(' ')[0] ##></td>
				<td>
					<button title="Remove" class="btn btn-danger btn-xs" ng-click="delete(account.bank_account_id)">
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					</button>
				</td>
			</tr>			
		</tbody>
	</table>
  </div>
</div>

@endsection