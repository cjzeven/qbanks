@extends('master.master')

@section('title', 'Register')

@section('content')

<div class="content" ng-controller="registerController">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="page-header">
					  <h1>Member Register</h1>
					</div>

					<!-- Notification -->		
					<div class="alert alert-danger notif" role="alert">
					  <ul>
					  	<li ng-repeat="error in errorData"><## error.message ##></li>
					  </ul>				  
					</div>	

					<div class="alert alert-success notif" role="alert">
						<span>Register success, redirecting...</span>				  
					</div>	

					<form id="form_register" method="post">
					  <div class="form-group">
					    <label class="control-label" for="firstname">Firstname</label>
					    <input name="firstname" type="text" class="form-control" id="firstname" placeholder="Firstname" ng-model="firstname">
					  </div>
					  <div class="form-group">
					    <label class="control-label" for="lastname">Lastname</label>
					    <input name="lastname" type="text" class="form-control" id="lastname" placeholder="Lastname" ng-model="lastname">
					  </div>
					  <div class="form-group">
					    <label class="control-label" for="username">Username</label>
					    <input name="username" type="text" class="form-control" id="username" placeholder="Username" ng-model="username">
					  </div>	
					  <div class="form-group">
					    <label class="control-label" for="email">Email</label>
					    <input name="email" type="email" class="form-control" id="email" placeholder="Email" ng-model="email">
					  </div>	
					  <div class="form-group">
					    <label class="control-label" for="password">Password</label>
					    <input name="password" type="password" class="form-control" id="password" placeholder="Password" ng-model="password">
					  </div>	
					  <div class="form-group">
					    <label class="control-label" for="password_confirm">Confirm Password</label>
					    <input name="password_confirm" type="password" class="form-control" id="password_confirm" placeholder="Confirm password" ng-model="confirmPassword">
					  </div>						  					  					  				  
					  <button id="register" type="submit" class="btn btn-primary" ng-click="register()">Register</button>
					  <a class="btn btn-danger pull-right" type="button" href="/">Cancel</a>
					</form>			
				</div>
			</div>
		</div>		
	</div>
</div>
@endsection