@section('menubar')
<div class="col-md-3">
	<ul class="nav nav-pills nav-stacked">
		 	<li role="presentation" class="@yield('history_active')">
		 	<a href="<?php echo url(); ?>/dashboard" class="tab_history"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> History</a>
		 	</li>
		  	<li role="presentation" class="@yield('balance_active')">
		  	<a href="<?php echo url(); ?>/balance" class="tab_balance"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span> Balance</a>
		  	</li>
		  	<li role="presentation" class="@yield('register_account_active')">
		  	<a href="<?php echo url(); ?>/register_account" class="tab_register_account"><span class="glyphicon glyphicon-flag" aria-hidden="true"></span> Register Bank</a>
		  	</li>		  	
		  	<li role="presentation" class="@yield('settings_active') dropdown">
			    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
			    <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
			      Manage &nbsp;<span class="caret"></span>
			    </a>
			    <ul class="dropdown-menu">
			    	<li><a href="<?php echo url(); ?>/settings/change_password">Change Password</a></li>
			    	<li><a href="<?php echo url(); ?>/settings/bank_account_lists">Delete Bank Account</a></li>
			    </ul>
		  	</li>		  	
		  	<li role="presentation" class="@yield('logout_active')">
		  	<a href="<?php echo url(); ?>/logout"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> Logout</a>
		  	</li>		 
	</ul>
</div>
@endsection