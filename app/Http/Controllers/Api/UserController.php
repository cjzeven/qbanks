<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Http\Validator\UserValidator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\EmailController as Email;
use App\Http\Models\User;
use App\Http\Models\PasswordResets as Forgot;

class UserController extends Controller
{
	private $validate, $response, $user, $email, $forgot;

    public function __construct(Request $request, UserValidator $validate)
    {
    	$this->validate = $validate;
    	$this->response = [
    		'request' => $request->all(),
    		'response' => [
    			'status' => false,
    			'message' => '',
    			'errors' => [],
    			'data' => []
    		]
    	];
    }

    public function register(Request $request)
    {
    	$validator = $this->validate->register($request->all());
    	
    	if( ! $validator['status']) {
    		$this->response['response']['errors'] = $validator['errors'];
    		$this->response['response']['message'] = $validator['message'];
    	
    		return $this->response;
    	}

    	$user = User::register($request);

        Email::welcome($user->email);
    	
    	$this->response['response']['status'] = true;
    	$this->response['response']['message'] = $validator['message'];
    	
    	return $this->response;
    }

    public function login(Request $request)
    {
    	$validator = $this->validate->login($request->all());
    	
    	if( ! $validator['status']) {
    		$this->response['response']['errors'] = $validator['errors'];
    		$this->response['response']['message'] = $validator['message'];
    	
    		return $this->response;
    	}
    	
    	$user = User::login($request);

    	if( ! $user) {
            $this->response['response']['errors']['password'] = 'Invalid username or password.';
    		$this->response['response']['message'] = 'Failed';
    		return $this->response;
    	}
    	
    	$this->response['response']['status'] = true;
    	$this->response['response']['message'] = $validator['message'];
    	$this->response['response']['data']['user'] = $user;
    	
    	return $this->response;
    }

    public function logout(Request $request)
    {
    	if(User::logout($request->user_id)) {
	    	$this->response['response']['status'] = true;
	    	$this->response['response']['message'] = 'Success.';

	    	return $this->response;   		
    	}

		$this->response['response']['message'] = 'User not found.';
		
		return $this->response;
    }

    public function forgot(Request $request)
    {
    	$validator = $this->validate->forget($request->all());
        
        if( ! $validator['status']) {
            $this->response['response']['errors'] = $validator['errors'];
            $this->response['response']['message'] = $validator['message'];
        
            return $this->response;
        }
        
        $token = bcrypt(Carbon::now());

        Forgot::resetToken($request->input('email'), $token); 

        Email::forgot($request->input('email'), $token);
        
        $this->response['response']['status'] = true;
        $this->response['response']['message'] = $validator['message'];
        
        return $this->response;
    }

    public function resetPassword(Request $request)
    {
        $validator = $this->validate->resetPassword($request->all());
        
        if( ! $validator['status']) {
            $this->response['response']['errors'] = $validator['errors'];
            $this->response['response']['message'] = $validator['message'];
        
            return $this->response;
        }
        
        $reset = User::resetPassword($request->input('token'), $request->input('new_password'));

        if( ! $reset) {
            $this->response['response']['message'] = 'Token Expired.';
            return $this->response;            
        }

        Forgot::deleteResetData($reset->reset_id);
        
        Email::passwordReseted($reset->email);
        
        $this->response['response']['status'] = true;
        $this->response['response']['message'] = $validator['message'];
        
        return $this->response;
    }

    public function checkToken(Request $request)
    {
        $user = User::checkToken($request->header('token'));
        if( ! User::checkTokenExpired($user->token_expired)) {
            $this->response['response']['errors']['token'] = 'Token expired.';
            $this->response['response']['message'] = 'Failed.';
            return $this->response;  
        } else {
            $this->response['response']['status'] = true;
            $this->response['response']['message'] = 'Success.';
            return $this->response;
        }
    }

    public function changePassword(Request $request)
    {
        $validator = $this->validate->changePassword($request->all());
        
        if( ! $validator['status']) {
            $this->response['response']['errors'] = $validator['errors'];
            $this->response['response']['message'] = $validator['message'];
        
            return $this->response;
        }        

        $user = User::changePassword($request->user_id, $request->input('password'), $request->input('new_password'));
        
        if( ! $user) {
            $this->response['response']['message'] = 'Change password failed.';
            return $this->response;                  
        }

        $this->response['response']['status'] = true;
        $this->response['response']['message'] = 'Success.';
        return $this->response;        

    }
}
