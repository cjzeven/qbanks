<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Validator\BankValidator;
use App\Http\Validator\CashflowValidator;
use App\Http\Models\BankAccount;
use App\Http\Models\Cashflow;
use Crypt;
use App\Http\Banks\Mandiri;
use Carbon\Carbon;

class CashflowController extends Controller
{
	private $response, $validate;

	public function __construct(Request $request, CashflowValidator $validate)
	{
		$this->validate = $validate;
        $this->response = [
            'request' => $request->all(),
            'response' => [
                'status' => false,
                'message' => '',
                'errors' => [],
                'data' => []
            ]
        ]; 
	}

	public function userDescription(Request $request)
	{
		$cashflowId = $request->input('cashflow_id');
		$description = $request->input('description');

		$cashflow = Cashflow::find($cashflowId);
		$cashflow->user_description = $description;
		
		if($cashflow->save()) {
            $this->response['response']['status'] = true;
            $this->response['response']['message'] = 'Success.';
            return $this->response;
		}

		return $this->response;
	}

	public function status(Request $request)
	{
		$cashflowId = $request->input('cashflow_id');
		$status = $request->input('status');
		
		$cashflow = Cashflow::find($cashflowId);
		$cashflow->status = $status;
		
		if($cashflow->save()) {
            $this->response['response']['status'] = true;
            $this->response['response']['message'] = 'Success.';
            return $this->response;
		}

		return $this->response;
	}

	public function search(Request $request)
	{
		$validator = $this->validate->search($request->all());
		
		if( ! $validator['status']) {
			$this->response['response']['errors'] = $validator['errors'];
			$this->response['response']['message'] = $validator['message'];
		
			return $this->response;
		}
		
		$cashflow = Cashflow::search($request->input('bank_account_id'), $request->input('from'), $request->input('to'));
		$bankAccount = BankAccount::find($request->input('bank_account_id'));
		
		$this->response['response']['data']['history'] = $cashflow;
		$this->response['response']['data']['account_number'] = $bankAccount->account_number;
		$this->response['response']['status'] = true;
		$this->response['response']['message'] = $validator['message'];
		
		return $this->response;		
	}
}