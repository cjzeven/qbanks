<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Validator\BankValidator;
use App\Http\Models\BankAccount;
use App\Http\Models\Cashflow;
use App\Http\Models\Balance;
use App\Http\Models\Bank;
use Crypt;
use App\Http\Banks\Mandiri;
use App\Http\Banks\Bni;
use Carbon\Carbon;
use App\Http\Libraries\Curl;

class BankController extends Controller
{
    private $validate, $response, $mandiri, $bni, $curl;

    public function __construct(BankValidator $validate, Request $request, Mandiri $mandiri, Bni $bni, Curl $curl)
    {
        $this->validate = $validate;
        $this->response = [
            'request' => $request->all(),
            'response' => [
                'status' => false,
                'message' => '',
                'errors' => [],
                'data' => []
            ]
        ];   
        $this->mandiri = $mandiri;
        $this->bni = $bni;
        $this->curl = $curl;
    }

    public function createSessionFile($id)
    {
        $filename = app_path('../resources/cookies/') . $id . '.txt';

        try {
            $cookieFile = file_put_contents($filename, '');            
        } catch (Exception $e) {
            $this->response['response']['message'] = 'Failed to create cookie file.';
            return $this->response;
        }

        $this->response['response']['message'] = 'Success.';
        $this->response['response']['status'] = true; 

        return $this->response;     
    }    

    public function register(Request $request)
    {
        $validator = $this->validate->register($request->all());
        
        if( ! $validator['status']) {
            $this->response['response']['errors'] = $validator['errors'];
            $this->response['response']['message'] = $validator['message'];
        
            return $this->response;
        }

        $account = BankAccount::checkIfAlreadyRegistered($request->user_id, $request->input('account_number'));

        if($account) {
            $this->response['response']['errors']['account_number'] = 'Bank account already registered.';
            $this->response['response']['message'] = 'Failed';
            return $this->response;            
        }   

        $register = BankAccount::register($request);
        
        if( ! $register) {         
            $this->response['response']['message'] = 'Failed to save account.';
            return $this->response;
        }

        $createSessionFile = $this->createSessionFile($register->id);

        if( ! $createSessionFile['response']['status']) {
            $this->response['response']['message'] = $createSessionFile['response']['message'];
            return $this->response;
        }                

        // Melakukan test login sebelum menyimpan ke database
        switch ($request->input('bank_id')) {
            case 1:
                $login = $this->mandiri->doLogin($register->id, $request->input('username'), $request->input('password'));
                break;

            case 2:
                $login = $this->bni->doLogin($register->id, $request->input('username'), $request->input('password'));
                break;
        }      

        if( ! $login['response']['status']) {
            // Jika gagal login
            // Hapus data register
            BankAccount::find($register->id)->delete();

            $this->response['response']['message'] = 'Failed';
            $this->response['response']['errors'][] = 'Please check your connection or your internet banking credentials.';
            return $this->response;            
        }  
        
        $this->response['response']['status'] = true; 
        $this->response['response']['message'] = 'Success.';
        $this->response['response']['data']['secret_key'] = $register->secret_key;

        return $this->response;
    }   

    public function init($request)
    {
        // cek last login
        $bankAccount = BankAccount::where('secret_key', $request->input('secret_key'))->first();
        $lastLogin = Carbon::parse($bankAccount->last_login);
        $now = Carbon::now();

        // Jika waktu antara last_login dan waktu sekarang lebih dari 3 menit
        // if($now->diffInMinutes($lastLogin) >= 1) {
        //     $login = $this->login($request);
        //     return $login['response']['status'];
        // } else {
        //     return true;
        // }

        $login = $this->login($request);
        return $login['response']['status'];
    }    

    public function login(Request $request) 
    {
        $validator = $this->validate->checkSecretKey($request->all());

        if( ! $validator['status']) {
            $this->response['response']['errors'] = $validator['errors'];
            $this->response['response']['message'] = $validator['message'];
        
            return $this->response;
        }     

        $this->logout($request);  

        $bankAccount = BankAccount::where('secret_key', $request->input('secret_key'))->first();
        $username = Crypt::decrypt($bankAccount->username);
        $password = Crypt::decrypt($bankAccount->password);

        switch ($bankAccount->bank_id) {
            case 1:
                $login = $this->mandiri->doLogin($bankAccount->id, $username, $password);
                break;

            case 2:
                $login = $this->bni->doLogin($bankAccount->id, $username, $password);
                break;
        }  

        // Update last login
        if($login['response']['status']) {
            $bankAccount = BankAccount::find($bankAccount->id);
            $bankAccount->last_login = Carbon::now();
            $bankAccount->save();
        }         

        return $login;
    } 

    // History baru
    public function transactionHistory(Request $request)
    {
        $validator = $this->validate->checkSecretKey($request->all());
        
        if( ! $validator['status']) {
            $this->response['response']['errors'] = $validator['errors'];
            $this->response['response']['message'] = $validator['message'];
        
            return $this->response;
        }
        
        if( ! $this->init($request)) {
            $this->response['response']['status'] = false;
            $this->response['response']['message'] = 'Initialize login failed.'; 
            $this->response['response']['errors'][] = 'Please check your internet connection.'; 

            return $this->response;           
        };

        // Date checker
        // $from = explode('/', $request->input('from'));
        // $fromParse = Carbon::parse($from[2] . '-' . $from[1] . '-' . $from[0]);
        // $to = explode('/', $request->input('to'));
        // $toParse = Carbon::parse($to[2] . '-' . $to[1] . '-' . $to[0]);

        // if((int) $fromParse->diffInDays($toParse) > 30) {
        //     $this->response['response']['status'] = false;
        //     $this->response['response']['message'] = 'Maximum 31 days range.'; 

        //     return $this->response; 
        // }

        $bankAccount = BankAccount::where('secret_key', $request->input('secret_key'))->first();

        switch ($bankAccount->bank_id) {
            case 1:
                $history = $this->mandiri->getTransactionHistory($bankAccount->id, $request);
                break;

            case 2:
                $history = $this->bni->getTransactionHistory($bankAccount->id, $request);
                break;
        }

        // Save transaction history
        $newHistory = $this->saveTransactionHistory($bankAccount->id, $history);

        // $newHistory = Cashflow::where('bank_account_id', $bankAccount->id)->get();

        // Return just new transaction history from database
        $history['response']['data']['history'] = $newHistory;     

        // Logout
        $this->logout($request);
        
        return $history;
    }

    // History lama
    public function transactionLastHistory(Request $request)
    {
        // $bankAccount = BankAccount::where('bank_account_id', $request->input('secret_key'))->first();
        $cashflow = Cashflow::where('bank_account_id', $request->input('bank_account_id'));

        if($request->input('order_by') == 'desc') {
            $cashflow->orderBy('date', 'desc');
        }

        if($request->input('limit')) {
            $cashflow->take($request->input('limit'));
        }

        if($request->input('offset')) {
            $cashflow->skip($request->input('offset'));
        }

        $cashflow = $cashflow->get();

        $bankAccount = BankAccount::where('id', $request->input('bank_account_id'))->first();

        if($cashflow) {
            $this->response['response']['status'] = true;
            $this->response['response']['message'] = 'Success';
        } else {
            $this->response['response']['message'] = 'No result';
            return $this->response;
        }

        $this->response['response']['data'] = $cashflow;   
        $this->response['response']['account_number'] = $bankAccount->account_number;   
        return $this->response;
    } 

    private function saveTransactionHistory($id, $history)
    {
        $data = [];
        $no = 0;

        foreach ($history['response']['data']['history'] as $row) {

            // Cek apakah ada data cashflow yang sudah di simpan
            if($this->checkRedundantCashflow($id, $row)) {
                $cashflow = new Cashflow;            
                $cashflow->bank_account_id = $id;
                $cashflow->description = $row['desc'];
                $cashflow->date = $row['date'];
                $cashflow->amount = $row['amount'];
                $cashflow->type = $row['type'];
                $cashflow->save();

                $data[$no] = $row;
            }
            
            // $data[$no] = $row;
            $no++;
        }

        return $data;
    }

    private function saveBalanceHistory($id, $data)
    {
        $balance = new Balance;
        $balance->bank_account_id = $id;
        $balance->amount = $data['response']['data']['account_balance'];
        
        if($balance->save()) {
            return true;
        }

        return false;
    }    

    private function checkRedundantCashflow($id, $cashflow)
    {
        $cashflow = Cashflow::where(['bank_account_id' => $id, 'description' => $cashflow['desc'], 'date' => $cashflow['date'], 'amount' => $cashflow['amount'], 'type' => $cashflow['type']])->first();

        if($cashflow) {
            return false;
        } else {
            return true;
        }
    }

    public function transactionCheck()
    {
    	return 'check transaction from api';
    }  

    public function balance(Request $request)
    {
        $validator = $this->validate->checkSecretKey($request->all());

        if( ! $validator['status']) {
            $this->response['response']['errors'] = $validator['errors'];
            $this->response['response']['message'] = $validator['message'];
        
            return $this->response;
        }

        if( ! $this->init($request)) {
            $this->response['response']['status'] = false;
            $this->response['response']['message'] = 'Initialize login failed.'; 

            return $this->response;           
        };

        $bankAccount = BankAccount::where('secret_key', $request->input('secret_key'))->first();

        switch ($bankAccount->bank_id) {
            case 1:
                $balance = $this->mandiri->getBalance($bankAccount->id);
                break;

            case 2:
                $balance = $this->bni->getBalance($bankAccount->id);
                break;
        }   

        if( ! $this->saveBalanceHistory($bankAccount->id, $balance)) {
            $balance['response']['status'] = false;
            $balance['response']['message'] = 'Failed to save to database.';
        }

        // Logout
        $this->logout($request);

        return $balance;         
    }

    public function logout(Request $request)
    {
        $validator = $this->validate->checkSecretKey($request->all());

        if( ! $validator['status']) {
            $this->response['response']['errors'] = $validator['errors'];
            $this->response['response']['message'] = $validator['message'];
        
            return $this->response;
        }

        $bankAccount = BankAccount::where('secret_key', $request->input('secret_key'))->first();

        switch ($bankAccount->bank_id) {
            case 1:
                $logout = $this->mandiri->doLogout($bankAccount->id);
                break;

            case 2:
                $logout = $this->bni->doLogout($bankAccount->id);
                break;
        }

        if($logout['response']['status']) {
            // Update last login
            $account = BankAccount::find($bankAccount->id);
            $account->last_login = '0000-00-00 00:00:00';
            $account->save();            

            // Clear cookie file
            $this->curl->setCookieName($bankAccount->id);
            $this->curl->clearCookieFile();
        }   

        return $logout;        
    }

    public function accounts(Request $request)
    {
        $bankAccount = BankAccount::with('bank')->where('user_id', $request->user_id);

        if( ! $bankAccount->get()) {
            $this->response['response']['message'] = 'No result';
            return $this->response;
        }        
        
        $banks = [];
        foreach ($bankAccount->get() as $k1 => $row) {
            $banks[$k1]['bank_account_id'] = $row->id;
            $banks[$k1]['secret_key'] = $row->secret_key;            
            $banks[$k1]['bank_accounts'] = $row->bank;
            $banks[$k1]['account_no'] = $row->account_number;
        }

        $this->response['response']['status'] = true;
        $this->response['response']['message'] = 'Success';
        $this->response['response']['data'] = $banks;
        return $this->response;
    }

    public function totalHistory(Request $request)
    {
        $history = Cashflow::where('bank_account_id', $request->input('bank_account_id'))->count();
        $this->response['response']['status'] = true;
        $this->response['response']['message'] = 'Success';
        $this->response['response']['data']['total_history'] = $history;
        return $this->response;        
    }

    public function balanceAmount(Request $request)
    {
        $balance = Balance::with('bankAccount')->where('bank_account_id', $request->input('bank_account_id'))
                            ->orderBy('id', 'desc')->first();

        if($balance) {
            $this->response['response']['status'] = true;
            $this->response['response']['message'] = 'Success';
            $this->response['response']['data'] = $balance;
            return $this->response;   
        } else {
            $this->response['response']['data']['balance'] = 0;
            $this->response['response']['data']['account_number'] = 0;            
            return $this->response;               
        }

    }

    public function balanceTotal(Request $request)
    {
        $bankAccountId = BankAccount::where('user_id', $request->user_id)->get();
        $amount = [];
        
        if($bankAccountId) {
            foreach ($bankAccountId as $key => $row) {
                $balance = Balance::where('bank_account_id', $row->id)->first();

                if($balance) {
                    $amount[] = $balance->amount;
                }

                $amount[] = 0;
            }
        }

        $amountTotal = array_sum($amount);
        $this->response['response']['status'] = true;
        $this->response['response']['message'] = 'Success';
        $this->response['response']['data']['total_balance'] = $amountTotal;
        return $this->response;    
    }

    public function balanceHistory(Request $request)
    {
        $balance = Balance::with('bankAccount')->where('bank_account_id', $request->input('bank_account_id'))
                            ->orderBy('created_at', 'desc')->groupBy(\DB::raw('DATE(updated_at), amount'))
                            ->take(50)->get();

        if($balance) {
            $this->response['response']['status'] = true;
            $this->response['response']['message'] = 'Success';
            $this->response['response']['data']['balance'] = $balance;
            return $this->response;  
        }

        $this->response['response']['data']['balance'] = [];
        return $this->response;               
        
    }

    public function banks(Request $request)
    {
        $banks = Bank::all();
        
        if($banks) {
            $this->response['response']['status'] = true;
            $this->response['response']['message'] = 'Success';
            $this->response['response']['data']['bank'] = $banks;
            return $this->response;
        }

        $this->response['response']['data']['bank'] = [];
        return $this->response;                
    }

    public function deleteAccount(Request $request)
    {
        $validator = $this->validate->deleteAccount($request->all());

        if( ! $validator['status']) {
            $this->response['response']['errors'] = $validator['errors'];
            $this->response['response']['message'] = $validator['message'];
        
            return $this->response;
        }        

        $bankAccount = BankAccount::find($request->input('bank_account_id'));

        if($bankAccount->delete()) {
            $this->response['response']['status'] = true;
            $this->response['response']['message'] = 'Success';
            return $this->response;            
        }

        $this->response['response']['message'] = 'Change password failed.';
        return $this->response;           
    }

}
