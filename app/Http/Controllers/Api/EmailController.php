<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class EmailController extends Controller
{
	public static function welcome($email)
	{
		try {
			Mail::send('emails.welcome', ['null' => null], function($message) use ($email) {
				$message->to($email)
						->from(env('MAIL_USERNAME'))
						->subject('[QBanks] Welcome to QBanks');
			});			
		} catch (Exception $e) {
			throw new Exception($e);
		}
	}

	public static function forgot($email, $token)
	{
		try {
			Mail::send('emails.forgot', ['token' => $token], function($message) use ($email) {
				$message->to($email)
						->from(env('MAIL_USERNAME'))
						->subject('[QBanks] Reset Your QBanks Password');
			});			
		} catch (Exception $e) {
			throw new Exception($e);
		}
	}

	public static function passwordReseted($email)
	{
		try {
			Mail::send('emails.reset', ['null' => null], function($message) use ($email) {
				$message->to($email)
						->from(env('MAIL_USERNAME'))
						->subject('[QBanks] Password Reset Successfully');
			});			
		} catch (Exception $e) {
			throw new Exception($e);
		}
	}	
}
