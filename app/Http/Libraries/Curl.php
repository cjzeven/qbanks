<?php

namespace App\Http\Libraries;

class Curl
{
    private $headers, $cookieDir, $cookieName;

    public function __construct()
    {
        $this->headers = [
            'Host: ibank.bni.co.id',
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            // 'Accept-Encoding: gzip, deflate, sdch',
            'Accept-Language: en-US,en;q=0.8',
            'Connection: gzip, keep-alive',
            'Upgrade-Insecure-Requests: 1',
            'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36',
            // 'Content-Type: application/x-www-form-urlencoded'
        ];
        $this->cookieDir = app_path('../resources/cookies/');
        $this->cookieName = null;
    }

    public function setCookieName($name)
    {
        $this->cookieName = $name;
        return $this->cookieName;
    }

    public function clearCookieFile()
    {
        file_put_contents($this->cookieDir . $this->cookieName . '.txt', '');
    }

    public function get($url, $option = [])
    {
        $ch = curl_init();
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => $this->headers,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_COOKIEJAR => $this->cookieDir . $this->cookieName . '.txt',
            CURLOPT_COOKIEFILE => $this->cookieDir . $this->cookieName . '.txt'         
        ];

        $options = $options + $option;
        curl_setopt_array($ch, $options);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    public function post($url, $option = [], $postfield)
    {
        $ch = curl_init();
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => $this->headers,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_COOKIEJAR => $this->cookieDir . $this->cookieName . '.txt',
            CURLOPT_COOKIEFILE => $this->cookieDir . $this->cookieName . '.txt',
            CURLOPT_POSTFIELDS => $postfield,          
        ];

        $options = $options + $option;
        curl_setopt_array($ch, $options);
        $data = curl_exec($ch);
        curl_close($ch);  

        return $data;
    }
}
