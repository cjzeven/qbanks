<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Models\User;
use Illuminate\Http\Request;

class TokenAuthenticate
{
    private $user, $excepts, $response;

    public function __construct(Request $request, User $user)
    {
        $this->user = $user;
        $this->excepts = [
            'api/user/forgot',
            'api/user/reset_password'
        ];
        $this->response = [
            'request' => $request->all(),
            'response' => [
                'status' => false,
                'message' => '',
                'errors' => [],
                'data' => []
            ]
        ]; 
    }  

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(in_array($request->path(), $this->excepts)) {
            return $next($request);
        }

        if( ! $request->header('token')) {
            $this->response['response']['errors'] = 'Please insert token.';
            $this->response['response']['message'] = 'Failed.';
            return $this->response;            
        }        

        $user = $this->user->checkToken($request->header('token'));

        if( ! $user) {
            $this->response['response']['errors']['token'] = 'Invalid token.';
            $this->response['response']['message'] = 'Failed.';
            return $this->response;             
        }

        if( ! $this->user->checkTokenExpired($user->token_expired)) {
            $this->response['response']['errors']['token'] = 'Token expired.';
            $this->response['response']['message'] = 'Failed.';
            return $this->response;  
        }        

        $request->user_id = $user->id;
        return $next($request);
    }
}
