<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Http\Models\User;

class ApiKeyAuthenticate
{
    private $user, $excepts, $response;

    public function __construct(Request $request, User $user)
    {
        $this->user = $user;
        $this->excepts = [
            'api/user/register',
            'api/user/login',
        ];
        $this->response = [
            'request' => $request->all(),
            'response' => [
                'status' => false,
                'message' => '',
                'errors' => [],
                'data' => []
            ]
        ]; 
    }  

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(in_array($request->path(), $this->excepts)) {
            return $next($request);
        }

        if($this->user->checkApiKey($request->header('api-key'))) {
            return $next($request);
        } else {
            $this->response['response']['message'] = 'Invalid api key.';
            return $this->response;            
        }

        $this->response['response']['message'] = 'Please insert api key.';
        return $this->response;
        
    }
}
