<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Crypt;
use Ramsey\Uuid\Uuid;

class BankAccount extends Model
{
    protected $table = 'bank_accounts';

    public static function register($request)
    {
        $account = new BankAccount;
        $account->user_id = $request->user_id;
        $account->bank_id = $request->input('bank_id');
        $account->username = Crypt::encrypt($request->input('username'));
        $account->password = Crypt::encrypt($request->input('password')); 
        $account->account_number = $request->input('account_number'); 
        $account->secret_key = Uuid::uuid4()->toString();
        
        if($account->save()) {
        	return $account;
        }

        return 0;
    }

    public static function checkIfAlreadyRegistered($userId, $accountNumber)
    {
        $account = BankAccount::where(['user_id' => $userId, 'account_number' => $accountNumber]);
        return $account->count();
    }

    public function bank()
    {
        return $this->belongsTo('App\Http\Models\Bank', 'bank_id');
    }
}
