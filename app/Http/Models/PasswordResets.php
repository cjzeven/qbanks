<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PasswordResets extends Model
{
    protected $table = 'password_resets';

    public static function resetToken($email, $token)
    {
    	$reset = PasswordResets::where('email', $email);

    	if($reset->count()) {
    		return $reset->update([
    				'email' => $email,
    				'reset_token' => $token,
    				'reset_token_expired' => Carbon::now()->addHours(env('RESET_TOKEN_EXPIRED'))
    			]); 
    	} else {
    		return $reset->insert([
    				'email' => $email,
    				'reset_token' => $token,
    				'reset_token_expired' => Carbon::now()->addHours(env('RESET_TOKEN_EXPIRED')),
    				'created_at' => Carbon::now(),
    				'updated_at' => Carbon::now()
    			]); 
    	}
    }

    public static function deleteResetData($id)
    {
    	$reset = PasswordResets::find($id);
    	$reset->delete();

    	return $reset;
    }
}
