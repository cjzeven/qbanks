<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Hash;

class User extends Model
{
    public static function generateToken($username)
    {
        return bcrypt($username . Carbon::now());
    }

    public static function generateTokenExpired()
    {
        return Carbon::now()->addHours(env('TOKEN_EXPIRED'))->toDateTimeString();
    }

	public static function register($request)
	{
    	$user = new User;
    	$user->firstname = $request->firstname;
    	$user->lastname = $request->lastname;
    	$user->username = $request->username;
    	$user->email = $request->email;
    	$user->password = bcrypt($request->password);
    	$user->save();

    	return $user;		
	}

    public static function checkApiKey($key)
    {
        return User::where('key', '=', $key)->count();
    }

    public static function login($request)
    {
        $user = User::where('username', $request->username)->first();

        if($user && Hash::check($request->password, $user->password)) {
            $user->token = User::generateToken($user->username);
            $user->token_expired = User::generateTokenExpired();
            $user->save();

            return $user;
        }

        return false;
    }

    public static function checkToken($token)
    {
        return User::where('token', '=', $token)->first();        
    }

    public static function checkTokenExpired($datetime)
    {
        $now = Carbon::now();
        return $now->lt(Carbon::parse($datetime));
    }

    public static function logout($id)
    {
        $user = User::find($id);

        if($user) {
            $user->token = null;
            $user->token_expired = null;
            $user->save();
        }

        return $user;
    }

    public static function resetPassword($token, $password)
    {
        $reset = User::select('users.*', 'password_resets.email', 'password_resets.reset_token', 
                            'password_resets.reset_token_expired', 'password_resets.id as reset_id')
                        ->join('password_resets', 'password_resets.email', '=', 'users.email')
                        ->where('password_resets.reset_token', $token)
                        ->first();

        if($reset) {
            if( ! User::checkTokenExpired($reset->reset_token_expired)) {
                return false;
            }

            $user = User::find($reset->id);
            $user->password = bcrypt($password);
            $user->token = null;
            $user->token_expired = null;
            $user->save();          
        }

        return $reset;
    }

    public static function changePassword($id, $password, $newPasword)
    {
        $user = User::find($id);
        
        if(Hash::check($password, $user->password)) {
            $user->password = bcrypt($newPasword);
            $user->token = null;
            $user->token_expired = null;
            
            if($user->save()) {
                return true;
            }            
        }

        return false;
    }
}
