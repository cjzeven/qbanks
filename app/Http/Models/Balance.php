<?php

namespace App\Http\Models;;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{

    public function bankAccount()
    {
        return $this->belongsTo('App\Http\Models\BankAccount', 'bank_account_id');
    }
}
