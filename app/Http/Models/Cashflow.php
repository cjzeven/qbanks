<?php

namespace App\Http\Models;;

use Illuminate\Database\Eloquent\Model;

class Cashflow extends Model
{
	public static function search($bankAccountId, $from, $to)
	{
		$cashflow = Cashflow::where("bank_account_id", "=", $bankAccountId)
							->where("date", ">=", $from)
							->where("date", "<=", $to)
							->orderBy("created_at", "desc");
		return $cashflow->get();
	}
}
