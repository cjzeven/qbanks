<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Crypt;
use Ramsey\Uuid\Uuid;

class BankAccountSetting extends Model
{
    protected $table = 'bank_account_settings';
}
