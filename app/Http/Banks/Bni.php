<?php

namespace App\Http\Banks;

use App\Http\Banks\Bank;
use App\Http\Libraries\Curl;
use phpQuery;
use App\Http\Models\BankAccountSetting;
use App\Http\Models\BankAccount;

class Bni implements Bank
{
    private $curl, $response, $sessionFile;

    public function __construct(Curl $curl)
    {
        $this->curl = $curl;
        $this->response = [
            'response' => [
                'status' => false,
                'message' => '',
                'data' => []
            ]
        ]; 
        $this->host = 'https://ibank.bni.co.id';
        $this->header = [
          'Host: ibank.bni.co.id',
          'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
          'Accept-Language: en-US,en;q=0.5',
        ];  
        $this->proxy = '127.0.0.1:8080';
        $this->enableProxy = true;      
    }

    private function saveSettings($id, $params)
    {
        // Save jsessionid to database
        if(BankAccountSetting::where(['bank_account_id' => $id])->first()) {
            BankAccountSetting::where(['bank_account_id' => $id])->update(['settings' => serialize($params)]);
        } else {
            $setting = new BankAccountSetting;
            $setting->bank_account_id = $id;
            $setting->settings = serialize($params);
            $setting->save();
        }              
    }    

    private function getMbParams($response)
    {
        // Mengambil mbparam
        $doc = phpQuery::newDocument($response);
        $mbparam = $doc->find('#mbparam')->val();
        $doc->unloadDocument();
        
        if($mbparam) {
            return $mbparam;
        } else {
            return 0;
        }
    }

    private function getACookie($response)
    {
        // Mengambil aCookie
        if(strpos($response, 'f5_cspm={f5_p:\'')) {
            $strStart = (strpos($response, 'f5_cspm={f5_p:\'')) + 15;
            $strEnd = strpos($response, '\',setCharAt:function');
            $aCookie = substr($response, $strStart, ($strEnd - $strStart));
            return $aCookie;          
        } else {
            return 0;
        }
    }

    public function doLogin($id, $username, $password)
    {
        $url = $this->host . '/MBAWeb/FMB';
        if($this->enableProxy) { $options[CURLOPT_PROXY] = $this->proxy; }
        $options[CURLOPT_SSL_VERIFYPEER] = 0;
        $options[CURLOPT_FOLLOWLOCATION] = true;
        $options[CURLOPT_HTTPHEADER] = $this->header;        
        
        $this->curl->setCookieName($id); 
        $response = $this->curl->get($url, $options);

        // Mengambil login url
        $doc = phpQuery::newDocument($response);
        $loginUrl = $doc->find('#RetailUser')->attr('href');
        $doc->unloadDocument();

        $this->curl->setCookieName($id); 
        $response = $this->curl->get($loginUrl, $options); 

        // Do Login
        $loginUrl2 = explode('?', $loginUrl);

        $postfield  = 'Num_Field_Err=%22Please+enter+digits+only%21%22&Mand_Field_Err=%22Mandatory+field+is+empty%21%22';
        $postfield .= '&CorpId='. $username .'&PassWord='. $password .'&__AUTHENTICATE__=Login&CancelPage=HomePage.xml&USER_TYPE=1';
        $postfield .= '&MBLocale=bh&language=bh&AUTHENTICATION_REQUEST=True&__JS_ENCRYPT_KEY__=&JavaScriptEnabled=N';
        $postfield .= '&deviceID=&machineFingerPrint=&deviceType=&browserType=&uniqueURLStatus=disabled';
        $postfield .= '&imc_service_page=SignOnRetRq&Alignment=LEFT&page=SignOnRetRq&locale=en';
        $postfield .= '&PageName=Thin_SignOnRetRq.xml&formAction='. $loginUrl2[0] .'&mConnectUrl=FMB&serviceType=Dynamic';  

        // $options[CURLOPT_FOLLOWLOCATION] = false;
        $this->curl->setCookieName($id);        
        $response = $this->curl->post($loginUrl2[0], $options, $postfield); 

        // Mengambil mbparam
        $mbparam = $this->getMbParams($response);

        // Push other params to array
        array_push($loginUrl2, $mbparam);

        // Pengecekan loginurl
        if( isset($loginUrl2[0]) && empty($loginUrl2[0])) {
            $this->response['response']['status'] = false;
            $this->response['response']['message'] = 'Failed.';     
            return $this->response;
        }

        // Save jsessionid to database
        $this->saveSettings($id, $loginUrl2);             

        if(strpos($response, 'Selamat Datang')) {
            $this->response['response']['status'] = true;
            $this->response['response']['message'] = 'Success.';
            $this->response['response']['data'] = $response;
        } else {
            $this->response['response']['status'] = false;
            $this->response['response']['message'] = 'Failed.';     
            // Lakukan log ketika login gagal               
        }

        return $this->response;
    }

    public function doLogout($id)
    {
        $setting = BankAccountSetting::where(['bank_account_id' => $id])->first();

        if($setting) {
            $setting = unserialize($setting->settings);
            if( ! isset($setting[0])) {
                $setting[0] = $this->host;
            } else if( ! isset($setting[2])) {
                $setting[2] = '';
            }
        } else {
            $setting[0] = $this->host;
            $setting[2] = '';
        }

        if($this->enableProxy) { $options[CURLOPT_PROXY] = $this->proxy; }
        $options[CURLOPT_SSL_VERIFYPEER] = 0;
        $options[CURLOPT_FOLLOWLOCATION] = true;
        $options[CURLOPT_HTTPHEADER] = $this->header;  
        $options[CURLOPT_REFERER] = $setting[0];          

        // Get Homepage
        $url = $setting[0] . '?page=LoginRs&DB_ACCOUNT_ID=%ACCOUNT_ID&mbparam=' . $setting[2];
        $this->curl->setCookieName($id);        
        $response = $this->curl->get($url, $options); 
        
        // Mengambil mbparam
        $mbparam = $this->getMbParams($response);  

        // Logout confirmation
        $options[CURLOPT_REFERER] = $url;

        $url = $setting[0];
        $postfield  = 'Num_Field_Err=%22Please+enter+digits+only%21%22&Mand_Field_Err=%22Mandatory+field+is+empty%21%22&LogOut=Keluar';
        $postfield .= '&mbparam='. $mbparam .'&uniqueURLStatus=disabled&imc_service_page=LoginRs&Alignment=LEFT';
        $postfield .= '&page=LoginRs&locale=bh&PageName=SignOnRetRq&formAction='. $url .'&mConnectUrl=FMB&serviceType=Dynamic';

        // $options[CURLOPT_FOLLOWLOCATION] = false;
        $this->curl->setCookieName($id);        
        $response = $this->curl->post($url, $options, $postfield);

        // Mengambil mbparam baru
        $newMbparam = $this->getMbParams($response);  

        $options[CURLOPT_REFERER] = $url;              

        // Logout action
        $postfield  = 'Num_Field_Err=%22Please+enter+digits+only%21%22&Mand_Field_Err=%22Mandatory+field+is+empty%21%22';
        $postfield .= '&__LOGOUT__=Keluar&mbparam='. $newMbparam .'&uniqueURLStatus=disabled&imc_service_page=SignOffUrlRq';
        $postfield .= '&Alignment=LEFT&page=SignOffUrlRq&locale=bh&PageName=LoginRs';
        $postfield .= '&formAction='. $url .'&mConnectUrl=FMB&serviceType=Dynamic';

        $this->curl->setCookieName($id);        
        $response = $this->curl->post($url, $options, $postfield);

        if(strpos($response, 'Anda telah berhasil ditandatangani.') || strpos($response, 'Anda telah berhasil keluar.')) {
            $this->response['response']['status'] = true;
            $this->response['response']['message'] = 'Success.';
            $this->response['response']['data'] = $response;
        } else {
            $this->response['response']['status'] = false;
            $this->response['response']['message'] = 'Failed.';
            // Lakukan log ketika login gagal               
        } 

        return $this->response;
    }

    public function getTransactionCheck()
    {
        return ['check transaction bni'];
    }

    public function getTransactionHistory($id, $request)
    {
        $setting = BankAccountSetting::where(['bank_account_id' => $id])->first();
        $setting = unserialize($setting->settings);  

        if($this->enableProxy) { $options[CURLOPT_PROXY] = $this->proxy; }
        $options[CURLOPT_SSL_VERIFYPEER] = 0;
        $options[CURLOPT_FOLLOWLOCATION] = true;
        $options[CURLOPT_HTTPHEADER] = $this->header;
        $options[CURLOPT_REFERER] = $setting[0];

        // Step 1
        $url = $setting[0] . '?page=AccountsUrlRq&mbparam=' . $setting[2];
        $this->curl->setCookieName($id);        
        $response = $this->curl->get($url, $options);

        // Mengambil mbparam
        $mbparam = $this->getMbParams($response);  

        // Step 2
        $options[CURLOPT_REFERER] = $url;

        $url = $setting[0] . '?page=TranHistoryRq&BBIDN=MM&mbparam=' . $mbparam;
        $this->curl->setCookieName($id);  
        $response = $this->curl->get($url, $options);

        // Mengambil mbparam
        $mbparam = $this->getMbParams($response);  

        // Step 3
        $options[CURLOPT_REFERER] = $url;

        $url = $setting[0];
        $postfield = 'Num_Field_Err=%22Please+enter+digits+only%21%22&Mand_Field_Err=%22Mandatory+field+is+empty%21%22&MAIN_ACCOUNT_TYPE=OPR&AccountIDSelectRq=Lanjut&AccountRequestType=Query&mbparam='. $mbparam .'&uniqueURLStatus=disabled&imc_service_page=AccountTypeSelectRq&Alignment=LEFT&page=AccountTypeSelectRq&locale=bh&PageName=TranHistoryRq&formAction='. $url .'&mConnectUrl=FMB&serviceType=Dynamic';        
        $this->curl->setCookieName($id);  
        $response = $this->curl->post($url, $options, $postfield);

        $doc = phpQuery::newDocument($response);
        $productName = $doc->find('#ACC_1_column3 > #acc3')->text();
        $accountNumber = $doc->find('#ACC_1_column2 > #acc2')->text();   
        $doc->unloadDocument(); 

        // Alternatif account number
        // Ambil dari database
        $bankAccount = BankAccount::where('id', $id)->first();
        $accountNumber = $bankAccount->account_number;

        // Mengambil mbparam
        $mbparam = $this->getMbParams($response);  

        // Step 4
        $options[CURLOPT_REFERER] = $url;

        // Mengambil history
        $dateFrom = explode('/', $request->input('from'));
        $dateTo = explode('/', $request->input('to'));
        $dateMap = ['01' => 'Jan', '02' => 'Feb', '03' => 'Mar', '04' => 'Apr', '05' => 'May', '06' => 'Jun', '07' => 'Jul', 
                '08' => 'Aug', '09' => 'Sep', '10' => 'Okt', '11' => 'Nov', '12' => 'Des'];
        $from = $dateFrom[2] . '-' . $dateMap[$dateFrom[1]] . '-' . $dateFrom[0];
        $to = $dateTo[2] . '-' . $dateMap[$dateTo[1]] . '-' . $dateTo[0];

        $postfield = 'Num_Field_Err=%22Please+enter+digits+only%21%22&Mand_Field_Err=%22Mandatory+field+is+empty%21%22&acc1=OPR%7C00000000193048214%7CBNI+TAPLUS&TxnPeriod=-1&Search_Option=Date&txnSrcFromDate='. $from .'&txnSrcToDate='. $to .'&FullStmtInqRq=Lanjut&MAIN_ACCOUNT_TYPE=OPR&mbparam='. $mbparam .'&uniqueURLStatus=disabled&imc_service_page=AccountIDSelectRq&Alignment=LEFT&page=AccountIDSelectRq&locale=bh&PageName=AccountTypeSelectRq&formAction='. $url .'&mConnectUrl=FMB&serviceType=Dynamic';        
        $this->curl->setCookieName($id);  
        $response = $this->curl->post($url, $options, $postfield);

        // Response
        if(strpos($response, 'Kami tidak dapat memproses permintaan anda')) {
            $this->response['response']['status'] = true;
            $this->response['response']['message'] = 'Success.';
            $this->response['response']['data'] = [
                'trx_number' => 0,
                'account_number' => $accountNumber,
                'account_type' => $productName,
                'bank_account_id' => $id,
                'history' => []
            ];
            // Log error
            return $this->response; 

        } else {
            // Mengambil mbparam
            $mbparam = $this->getMbParams($response);  

            // assign new mbparam
            $setting[2] = $mbparam;

            // Save settings to database
            $this->saveSettings($id, $setting);        

            // Mengambil jumlah data history
            $doc = phpQuery::newDocument($response);
            $totalHistoryRaw = $doc->find('#ResultsDisplayStringTag')->text();
            $totalHistoryEx = explode(' ', $totalHistoryRaw);
            $totalHistory = trim(end($totalHistoryEx));
            $doc->unloadDocument();

            // cek apakah ada pagination
            $doc = phpQuery::newDocument($response);
            $countPagination = count($doc->find('#Pagination'));
            $doc->unloadDocument();  

            if($countPagination) {
                // Mengambil elemen spesifik
                $doc = phpQuery::newDocument($response);
                $countData = count($doc->find('#Pagination > table > tr:gt(0)')) - 3;
                $data[] = $doc->find('#Pagination > table > tr:gt(0):lt('. $countData .')')->html();
                $doc->unloadDocument();  

                // Algoritma pagination
                $totalPagination = ceil($totalHistory / 10) - 1;

                for($i = 1; $i <= $totalPagination; $i++) {

                    $setting2 = BankAccountSetting::where(['bank_account_id' => $id])->first();
                    $setting2 = unserialize($setting2->settings);     

                    // Step 5
                    $url = $setting2[0] . '?page=AccountIDSelectRq&Paginated=true&StartValue=' . $i . '0';
                    
                    $options[CURLOPT_REFERER] = $setting2[0];

                    $this->curl->setCookieName($id);  
                    $response = $this->curl->get($url, $options); 

                    // Mengambil elemen spesifik
                    $doc = phpQuery::newDocument($response);
                    $countData = count($doc->find('#Pagination > table > tr:gt(0)')) - 3;
                    $data[] = $doc->find('#Pagination > table > tr:gt(0):lt('. $countData .')')->html();
                    $doc->unloadDocument();  

                    // Mengambil mbparam
                    $mbparam = $this->getMbParams($response);   

                    // assign new mbparam
                    $setting2[2] = $mbparam;

                    // Save settings to database
                    $this->saveSettings($id, $setting2);              
                }  
            } else {
                // Mengambil elemen spesifik
                $doc = phpQuery::newDocument($response);
                $countData = count($doc->find('table > tr:gt(5)')) - 8;
                $data[] = $doc->find('table > tr:gt(5):lt('. $countData .')')->html();
                $doc->unloadDocument(); 
            }            

            // Looping data history
            $no = 0;
            $rawHistory = [];
            foreach ($data as $key => $row) {
                $doc = phpQuery::newDocument($row);
                foreach ($doc->find('td') as $key => $row2) {
                    if(pq($row2)->find('#H')->text() != '') {
                        $rawHistory[$no][] = pq($row2)->find('#H')->text();
                    }
                    if(pq($row2)->find('#Header')->text() == '') {
                        $no++;
                    }
                }
                $no++;
                $doc->unloadDocument();  
            }                   

            // Grouping history
            $groupHistory = [];
            foreach ($rawHistory as $key1 => $row) {
                foreach ($row as $key2 => $row2) {
                    if($key2 == 0) {
                        $date = explode('-', $row2);
                        $groupHistory[$key1]['date'] = $date[2] . '-' . array_search($date[1], $dateMap) . '-' . $date[0];
                    } else if($key2 == 1) {
                        $groupHistory[$key1]['desc'] = $row2;
                    } else if($key2 == 2) {
                        $groupHistory[$key1]['type'] = strtoupper($row2);
                    } else if($key2 == 3) {
                        $amount = str_replace('.', '', explode(' ', $row2)[1]);
                        $groupHistory[$key1]['amount'] = str_replace(',', '.', $amount);
                    }                    
                }
            }

            if($request->input('order_by') == 'asc') {
                $groupHistoryEx = array_reverse($groupHistory);
                $groupHistory = $groupHistoryEx;
            }          

            if($groupHistory) {
                $this->response['response']['status'] = true;
                $this->response['response']['message'] = 'Success.';
                $this->response['response']['data'] = [
                    'trx_number' => 0,
                    'account_number' => $accountNumber,
                    'account_type' => $productName,
                    'bank_account_id' => $id,
                    'history' => $groupHistory
                ];               
            }

            return $this->response;            
        }

    }

    public function getBalance($id)
    {
        $setting = BankAccountSetting::where(['bank_account_id' => $id])->first();
        $setting = unserialize($setting->settings);  

        if($this->enableProxy) { $options[CURLOPT_PROXY] = $this->proxy; }
        $options[CURLOPT_SSL_VERIFYPEER] = 0;
        $options[CURLOPT_FOLLOWLOCATION] = true;
        $options[CURLOPT_HTTPHEADER] = $this->header;
        $options[CURLOPT_REFERER] = $setting[0];

        // Step 1
        $url = $setting[0] . '?page=AccountsUrlRq&mbparam=' . $setting[2];
        $this->curl->setCookieName($id);        
        $response = $this->curl->get($url, $options);

        // Mengambil mbparam
        $mbparam = $this->getMbParams($response);  

        // Step 2
        $options[CURLOPT_REFERER] = $url;

        $url = $setting[0] . '?page=BalanceInqRq&BBIDN=MM&mbparam=' . $mbparam;
        $this->curl->setCookieName($id);  
        $response = $this->curl->get($url, $options);

        // Mengambil mbparam
        $mbparam = $this->getMbParams($response);  

        // Step 3
        $options[CURLOPT_REFERER] = $url;

        $url = $setting[0];
        $postfield = 'Num_Field_Err=%22Please+enter+digits+only%21%22&Mand_Field_Err=%22Mandatory+field+is+empty%21%22&MAIN_ACCOUNT_TYPE=OPR&AccountIDSelectRq=Lanjut&AccountRequestType=ViewBalance&mbparam='. $mbparam .'&uniqueURLStatus=disabled&imc_service_page=AccountTypeSelectRq&Alignment=LEFT&page=AccountTypeSelectRq&locale=bh&PageName=BalanceInqRq&formAction='. $url .'&mConnectUrl=FMB&serviceType=Dynamic';        
        $this->curl->setCookieName($id);  
        $response = $this->curl->post($url, $options, $postfield);

        // Mengambil mbparam
        $mbparam = $this->getMbParams($response);  

        // Mengambil no rekening
        $doc = phpQuery::newDocument($response);
        $accountNo = $doc->find('#acc1_0')->val();
        $doc->unloadDocument();

        // Step 4
        $options[CURLOPT_REFERER] = $setting[0];

        $url = $setting[0];
        $postfield = 'Num_Field_Err=%22Please+enter+digits+only%21%22&Mand_Field_Err=%22Mandatory+field+is+empty%21%22&acc1='. $accountNo .'&BalInqRq=Lanjut&MAIN_ACCOUNT_TYPE=OPR&mbparam='. $mbparam .'&uniqueURLStatus=disabled&imc_service_page=AccountIDSelectRq&Alignment=LEFT&page=AccountIDSelectRq&locale=bh&PageName=AccountTypeSelectRq&formAction='. $url .'&mConnectUrl=FMB&serviceType=Dynamic';        
        $this->curl->setCookieName($id);  
        $response = $this->curl->post($url, $options, $postfield);

        // Mengambil mbparam
        $mbparam = $this->getMbParams($response);        

        // assign new mbparam
        $setting[2] = $mbparam;

        // Save settings to database
        $this->saveSettings($id, $setting);

        if(strpos($response, 'INFORMASI SALDO')) {
            $doc = phpQuery::newDocument($response);
            $accountNumber = $doc->find('#Row1_1_column2:first .BodytextCol2')->text();
            $accountType = $doc->find('#Row4_4_column2:first .BodytextCol2')->text();
            $balance = str_replace('.', '', $doc->find('#Row5_5_column2:eq(1) > .BodytextUnbold')->text());
            $balance = str_replace(',', '.', $balance);
            $doc->unloadDocument();

            $this->response['response']['status'] = true;
            $this->response['response']['message'] = 'Success.';
            $this->response['response']['data'] = [
                // 'account_number' => $accountNumber,
                'account_number' => BankAccount::where('id', $id)->first()->account_number,
                'account_type' => $accountType,
                'account_balance' => $balance,
            ]; 
        } else {
            $this->response['response']['status'] = false;
            $this->response['response']['message'] = 'Failed.'; 
            $this->response['response']['data'] = $response;
            // Log error                       
        }

        return $this->response; 
    }    
}
