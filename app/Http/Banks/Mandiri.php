<?php

namespace App\Http\Banks;

use App\Http\Banks\Bank;
use App\Http\Libraries\Curl;
use phpQuery;

class Mandiri implements Bank
{
    private $curl, $response, $sessionFile;

    public function __construct(Curl $curl)
    {
        $this->curl = $curl;
        $this->response = [
            'response' => [
                'status' => false,
                'message' => '',
                'data' => []
            ]
        ]; 
        $this->host = 'https://ib.bankmandiri.co.id';
        $this->header = [
          'Host: ib.bankmandiri.co.id',
          'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
          'Accept-Language: en-US,en;q=0.5'
        ];  
        $this->proxy = '127.0.0.1:8080';  
        $this->enableProxy = true;    
    }

    public function doLogin($id, $username, $password)
    {
        $url = $this->host . '/retail/Login.do?action=form&lang=in_ID';
        if($this->enableProxy) { $options[CURLOPT_PROXY] = $this->proxy; }
        $options[CURLOPT_SSL_VERIFYPEER] = 0;
        $options[CURLOPT_FOLLOWLOCATION] = true;
        $options[CURLOPT_HTTPHEADER] = $this->header;                
        
        $this->curl->setCookieName($id); 
        $response = $this->curl->get($url, $options);

        $url = $this->host . '/retail/Login.do';
        $options[CURLOPT_FOLLOWLOCATION] = false;              
        $postfield = 'action=result&userID='. $username .'&password='. $password .'&image.x=0&image.y=0';
        
        $this->curl->setCookieName($id);        
        $response = $this->curl->post($url, $options, $postfield);

        // Cek apakah berhasil login ke bank
        $url = $this->host . '/retail/Welcome.do?action=result';
        $this->curl->setCookieName($id); 
        $response = $this->curl->get($url, $options);     

        if(strpos($response, 'Login terakhir Anda adalah tanggal')) {
            $this->response['response']['status'] = true;
            $this->response['response']['message'] = 'Success.';
            $this->response['response']['data'] = $response;
        } else {
            $this->response['response']['status'] = false;
            $this->response['response']['message'] = 'Failed.';     
            // Lakukan log ketika login gagal       
        } 
        
        return $this->response;
    }

    public function getTransactionCheck()
    {
        return 'check transaction from api';
    }

    public function getTransactionHistory($id, $request)
    {
        $referer = 'https://ib.bankmandiri.co.id/retail/common/menu.jsp';
        $url = $this->host . '/retail/TrxHistoryInq.do?action=form';
        $options[CURLOPT_REFERER] = $referer;
        $options[CURLOPT_HTTPHEADER] = $this->header;
        if($this->enableProxy) { $options[CURLOPT_PROXY] = $this->proxy; }
        $options[CURLOPT_SSL_VERIFYPEER] = 0;

        $this->curl->setCookieName($id); 
        $response = $this->curl->get($url, $options); 

        // Mengambil account Id
        $doc = phpQuery::newDocument($response);
        $accountId = $doc->find('select[name=fromAccountID] option:last')->val();
        $doc->unloadDocument();

        $referer = 'https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do?action=form';
        $url = $this->host . '/retail/TrxHistoryInq.do';
        $options[CURLOPT_REFERER] = $referer;   
        
        // Mengambil history
        $dateFrom = explode('/', $request->input('from'));
        $dateTo = explode('/', $request->input('to'));
        $fromDay = $dateFrom[2];
        $fromMonth = $dateFrom[1];
        $fromYear = $dateFrom[0];
        $toDay = $dateTo[2];
        $toMonth = $dateTo[1];
        $toYear = $dateTo[0];
        $sortBy = ucfirst('date'); // Date, AllDescription, Balance
        $orderBy = strtoupper('asc'); // ASC, DESC
        $postfield = 'action=result&fromAccountID='. $accountId .'&searchType=R'
                    . '&fromDay='. $fromDay .'&fromMonth='. $fromMonth .'&fromYear='. $fromYear 
                    . '&toDay='. $toDay .'&toMonth=' . $toMonth
                    . '&toYear='. $toYear .'&sortType='. $sortBy .'&orderBy=' . $orderBy;

        $this->curl->setCookieName($id); 
        $response = $this->curl->post($url, $options, $postfield);

        $doc = phpQuery::newDocument($response);
        $trxNumber = $doc->find('table:eq(3) tr:first td:last')->text();
        $accNumber = $doc->find('table:eq(3) tr:eq(1) td:last')->text();
        $accType = $doc->find('table:eq(3) tr:eq(2) td:last')->text();
        $history = $doc->find('table:eq(4)')->html();
        // $startBalance = str_replace('.', '', trim($doc->find('table:eq(5) tr:eq(1) td:last span')->text()));
        // $endBalance = str_replace('.', '', trim($doc->find('table:eq(5) tr:last td:last span')->text()));
        // $debtTotal = str_replace('.', '', trim($doc->find('table:eq(5) tr:eq(3) td:last span')->text()));
        // $creditTotal = str_replace('.', '', trim($doc->find('table:eq(5) tr:eq(2) td:last span')->text()));

        if(pq($doc->find('table:eq(4) tr:last td'))->text() == 'Tidak ditemukan catatan') {
            $this->response['response']['status'] = true;
            $this->response['response']['message'] = 'Success.';
            $data = [];
        } else {
            $no = 0;
            foreach ($doc->find('table:eq(4) tr') as $k1 => $tr) {
                if($k1 > 0) {
                    if(count(pq($tr)->find('td')) > 1) {
                        $date = explode('/', str_replace('<br>', ' ', pq($tr)->find('td:eq(0)')->html()));
                        $data[$no]['date'] = $date[2] . '-' . $date[1] . '-' . $date[0];
                        $data[$no]['desc'] = trim(str_replace('<br>', ' ', pq($tr)->find('td:eq(1)')->html()));
                        $debt = str_replace('.', '', str_replace('<br>', ' ', pq($tr)->find('td:eq(2)')->html()));
                        $credit = str_replace('.', '', str_replace('<br>', ' ', pq($tr)->find('td:eq(3)')->html()));

                        if($debt == '0,00' && $credit !== '0,00') {
                            $data[$no]['type'] = 'CR';
                            $data[$no]['amount'] = str_replace(',', '.', $credit);
                        } else if ($debt !== '0,00' && $credit == '0,00') {
                            $data[$no]['type'] = 'DB';
                            $data[$no]['amount'] = str_replace(',', '.', $debt);
                        }
                    }
                    $no++;                    
                }
            }
        }

        $doc->unloadDocument(); 

        $this->response['response']['status'] = true;
        $this->response['response']['message'] = 'Success.';
        $this->response['response']['data'] = [
            'trx_number' => $trxNumber,
            'account_number' => explode(' ', $accNumber)[0],
            'account_type' => $accType,
            'bank_account_id' => $id,
            // 'summary' => [
            //     'start_balance' => str_replace(',', '.', $startBalance),
            //     'credit_total' => str_replace(',', '.', $creditTotal),
            //     'debt_total' => str_replace(',', '.', $debtTotal),
            //     'end_balance' => str_replace(',', '.', $endBalance)
            // ],
            'history' => $data
        ];

        return $this->response; 
    }

    public function getBalance($id)
    {
        $referer = $this->host . '/retail/common/menu.jsp';
        $url = $this->host . '/retail/AccountList.do?action=acclist';
        $options[CURLOPT_REFERER] = $referer;
        $options[CURLOPT_HTTPHEADER] = $this->header;
        if($this->enableProxy) { $options[CURLOPT_PROXY] = $this->proxy; }
        $options[CURLOPT_SSL_VERIFYPEER] = 0;        

        $this->curl->setCookieName($id); 
        $response = $this->curl->get($url, $options); 

        // Mengambil Account ID
        $strStart = (strpos($response, 'onchange="sendForm(')) + 20;
        $strEnd = strpos($response, '\',this)">');
        $accountId = substr($response, $strStart, ($strEnd - $strStart));

        if( ! $accountId) {
            // Lakukan log ketika login gagal             
            $this->response['response']['status'] = false;
            $this->response['response']['message'] = 'Failed.';   
            return $this->response;
        }

        // Mengambil balance/saldo
        $url = $this->host . '/retail/AccountDetail.do?action=result&ACCOUNTID=' . $accountId;
        $referer = $this->host . '/retail/AccountList.do?action=acclist';

        $this->curl->setCookieName($id); 
        $response = $this->curl->get($url, $options); 

        $doc = phpQuery::newDocument($response);
        $balance = $doc->find('table:eq(0) > tr:eq(3) > td:eq(1) > div > table:eq(1) > tr:eq(4) > td:last')->text();
        $balance = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', trim($balance));
        $balance = str_replace('.', '', substr($balance, 3));
        $balance = str_replace(',', '.', $balance);
        $accountNumber = $doc->find('table:eq(0) > tr:eq(3) > td:eq(1) > div > table:eq(1) > tr:eq(1) > td:last')->text();
        $accountType = $doc->find('table:eq(0) > tr:eq(3) > td:eq(1) > div > table:eq(1) > tr:eq(2) > td:last')->text();
        $doc->unloadDocument();

        $this->response['response']['status'] = true;
        $this->response['response']['message'] = 'Success.';
        $this->response['response']['data'] = [
            'account_number' => $accountNumber,
            'account_type' => $accountType,
            'account_balance' => $balance,
        ];

        return $this->response;    
    }    

    public function doLogout($id)
    {
        $referer = $this->host . '/retail/common/banner.jsp';
        $url = $this->host . '/retail/Logout.do?action=result';
        $options[CURLOPT_REFERER] = $referer;
        $options[CURLOPT_HTTPHEADER] = $this->header;
        if($this->enableProxy) { $options[CURLOPT_PROXY] = $this->proxy; }
        $options[CURLOPT_SSL_VERIFYPEER] = 0;        

        $this->curl->setCookieName($id); 
        $response = $this->curl->get($url, $options); 

        $this->response['response']['status'] = true;
        $this->response['response']['message'] = 'Success.';
        $this->response['response']['data'] = $response;

        return $this->response;     
    }
}
