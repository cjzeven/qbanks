<?php

namespace App\Http\Banks;

interface Bank
{
    public function getTransactionCheck();
    public function getTransactionHistory($id, $request);
    public function getBalance($id);
    public function doLogin($id, $username, $password);
    public function doLogout($id);
}
