<?php

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/login', ['as' => 'login', function() {
	return view('frontend.login');
}]);

Route::get('/logout', ['as' => 'logout', function() {
	return view('frontend.logout');
}]);

Route::get('/register', ['as' => 'register', function() {
	return view('frontend.register');
}]);

Route::get('/forgot', ['as' => 'forgot', function() {
	return view('frontend.forgot');
}]);

Route::get('/reset', ['as' => 'reset', function() {
	return view('frontend.reset');
}]);

Route::get('/register_account', ['as' => 'register_account', function() {
	return view('frontend.register_account');
}]);

Route::get('/dashboard', ['as' => 'dashboard', function() {
	return view('frontend.dashboard');
}]);

Route::get('/history', ['as' => 'history', function() {
	return redirect()->route('dashboard');
}]);

Route::get('/balance', ['as' => 'balance', function() {
	return view('frontend.balance');
}]);

Route::get('/settings/change_password', ['as' => 'change_password', function() {
	return view('frontend.change_password');
}]);

Route::get('/settings/bank_account_lists', ['as' => 'bank_account_lists', function() {
	return view('frontend.bank_account_lists');
}]);

Route::group(['prefix' => 'api'], function() {
	Route::group(['prefix' => 'user', 'middleware' => 'auth.api'], function() {
		Route::post('register', 'Api\UserController@register');
		Route::post('login', 'Api\UserController@login');
	});

	Route::group(['prefix' => 'user', 'middleware' => 'auth.token'], function() {
		Route::get('logout', 'Api\UserController@logout');
		Route::post('forgot', 'Api\UserController@forgot');
		Route::get('reset_password', 'Api\UserController@resetPassword');
		Route::post('check_token', 'Api\UserController@checkToken');
		Route::post('change_password', 'Api\UserController@changePassword');
	});	

	Route::group(['prefix' => 'bank', 'middleware' => 'auth.token'], function() {
		Route::group(['prefix' => 'transaction'], function() {
			Route::get('check', 'Api\BankController@transactionCheck');
			Route::get('history', 'Api\BankController@transactionHistory');
			Route::get('history/total', 'Api\BankController@totalHistory');
			Route::get('last_history', 'Api\BankController@transactionLastHistory');
		});	
		Route::get('/', 'Api\BankController@banks');		
		Route::get('balance', 'Api\BankController@balance');
		Route::post('register', 'Api\BankController@register');
		Route::post('login', 'Api\BankController@login');
		Route::get('logout', 'Api\BankController@logout');
		Route::get('accounts', 'Api\BankController@accounts');		
		Route::get('balance/amount', 'Api\BankController@balanceAmount');		
		Route::get('balance/total', 'Api\BankController@balanceTotal');		
		Route::get('balance/history', 'Api\BankController@balanceHistory');	
		Route::get('account/delete', 'Api\BankController@deleteAccount');					

		Route::group(['prefix' => 'cashflow'], function() {
			Route::post('user_description', 'Api\CashflowController@userDescription');
			Route::post('status', 'Api\CashflowController@status');
			Route::get('search', 'Api\CashflowController@search');
		});
	});
});
