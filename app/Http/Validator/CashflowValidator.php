<?php

namespace App\Http\Validator;

use Validator;

class CashflowValidator
{
	public function search($request)
	{
        $response = [
            'status' => false,
            'message' => 'Success.',
            'errors' => []
        ];  
              
        $validator = Validator::make($request, [
            'bank_account_id' => 'required|integer|exists:bank_accounts,id',            
            'from' => 'required|date_format:Y/m/d',
            'to' => 'required|date_format:Y/m/d',
        ]);

        if($validator->fails()) {
            $response['errors'] = $validator->errors();
            $response['message'] = 'Validator not valid.';
            return $response;
        }

        $response['status'] = true;
        return $response;		
	}
}

?>