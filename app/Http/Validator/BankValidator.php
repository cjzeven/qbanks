<?php

namespace App\Http\Validator;

use Validator;

class BankValidator
{  
    public function register($request)
    {
        $response = [
            'status' => false,
            'message' => 'Success.',
            'errors' => []
        ];  
              
        $validator = Validator::make($request, [
            'bank_id' => 'required|exists:banks,id',            
            'username' => 'required|alpha_dash|min:3|max:20',
            'password' => 'required',
            'account_number' => 'required|numeric',
        ]);

        if($validator->fails()) {
            $response['errors'] = $validator->errors();
            $response['message'] = 'Validator not valid.';
            return $response;
        }

        $response['status'] = true;
        return $response;
    }   

    public function checkSecretKey($request)
    {
        $response = [
            'status' => false,
            'message' => 'Success.',
            'errors' => []
        ];  
              
        $validator = Validator::make($request, [
            'secret_key' => 'required|exists:bank_accounts,secret_key',
        ]);

        if($validator->fails()) {
            $response['errors'] = $validator->errors();
            $response['message'] = 'Validator not valid.';
            return $response;
        }

        $response['status'] = true;
        return $response;
    }        

    public function deleteAccount($request)
    {
        $response = [
            'status' => false,
            'message' => 'Success.',
            'errors' => []
        ];  
              
        $validator = Validator::make($request, [
            'bank_account_id' => 'required|exists:bank_accounts,id|integer',
        ]);

        if($validator->fails()) {
            $response['errors'] = $validator->errors();
            $response['message'] = 'Validator not valid.';
            return $response;
        }

        $response['status'] = true;
        return $response;        
    } 
}
