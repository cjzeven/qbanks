<?php

namespace App\Http\Validator;

use Validator;

class UserValidator
{
    public function register($request)
    {
        $response = [
            'status' => false,
            'message' => 'Success.',
            'errors' => []
        ];  
              
    	$validator = Validator::make($request, [
            'firstname' => 'required|max:50',
            'lastname' => 'required|max:50',
            'username' => 'required|alpha_dash|min:5|max:12|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'password_confirm' => 'required|same:password',
        ]);

        if($validator->fails()) {
            $response['errors'] = $validator->errors();
            $response['message'] = 'Validator not valid.';
            return $response;
        }

        $response['status'] = true;
        return $response;
    }

    public function login($request)
    {
        $response = [
            'status' => false,
            'message' => 'Success.',
            'errors' => []
        ];  
              
        $validator = Validator::make($request, [
            'username' => 'required|alpha_dash|min:5|max:12',
            // 'email' => 'required_without:username|email',
            'password' => 'required'
        ]);

        if($validator->fails()) {
            $response['errors'] = $validator->errors();
            $response['message'] = 'Validator not valid.';
            return $response;
        }

        $response['status'] = true;
        return $response;
    }

    public function forget($request)
    {
        $response = [
            'status' => false,
            'message' => 'Success.',
            'errors' => []
        ];  
              
        $validator = Validator::make($request, [
            'email' => 'required|email|exists:users,email'
        ]);

        if($validator->fails()) {
            $response['errors'] = $validator->errors();
            $response['message'] = 'Validator not valid.';
            return $response;
        }

        $response['status'] = true;
        return $response;
    }    

    public function resetPassword($request)
    {
        $response = [
            'status' => false,
            'message' => 'Success.',
            'errors' => []
        ];  
              
        $validator = Validator::make($request, [
            'token' => 'required|exists:password_resets,reset_token',
            'new_password' => 'required',
            'new_password_confirm' => 'required|same:new_password'
        ]);

        if($validator->fails()) {
            $response['errors'] = $validator->errors();
            $response['message'] = 'Validator not valid.';
            return $response;
        }

        $response['status'] = true;
        return $response;
    }     

    public function changePassword($request)
    {
        $response = [
            'status' => false,
            'message' => 'Success.',
            'errors' => []
        ];  
              
        $validator = Validator::make($request, [
            'password' => 'required',
            'new_password' => 'required',
            'new_password_confirm' => 'required|same:new_password'
        ]);

        if($validator->fails()) {
            $response['errors'] = $validator->errors();
            $response['message'] = 'Validator not valid.';
            return $response;
        }

        $response['status'] = true;
        return $response;
    }             
}
