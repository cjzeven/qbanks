(function() {

	var qbanks = angular.module('qbanksApp', [], function($interpolateProvider) {
		$interpolateProvider.startSymbol('<##');
		$interpolateProvider.endSymbol('##>');
	});

	var url = document.origin;

	var loading = function(selector) {
		$(selector).block({
			message: '<img src="../assets/img/loading.gif" width="40px;"/>',
			css: {
				border: '0px',
				backgroundColor: 'transparent',
				color: '#333'
			},
			overlayCSS: {
		        backgroundColor: '#000',
		        opacity: 0.2,
		        cursor: 'wait'
		    }
		});
	}

	var unloading = function(selector) {
		$(selector).unblock();
	}

	// Find cookie in session
	var getCookie = function (name) {
	  	var value = "; " + document.cookie;
	  	var parts = value.split("; " + name + "=");
	  	if (parts.length == 2) return parts.pop().split(";").shift();
	}

	var loadingArea = function(selector) {
		$(selector).html('<img src="../assets/img/loading.gif" width="15px;"/>');	
	}

	var unloading = function(selector) {
		$(selector).unblock(); 
	}

	// Create datatable plugin
	var createDataTable = function(selector) {
		$(selector).dataTable({
			'pagingType' : 'simple',
			'scrollCollapse' : false,
			// 'scrollY' : '350',
			'sorting': false,
		});
	}

	// Login Controller
	qbanks.controller('loginController', function($scope, $http) {

		$scope.login = function() {

			$('.alert').hide();
			$('input.form-control').parent().parent().removeClass('has-error');		

			$scope.errorData = [];
			$scope.status = false;

			var onSuccess = function(r) {
				// console.log(r);
				if(r.data.response.status) {
					$scope.status = ! $scope.status;
					// remove notification alert
					$('div.alert-danger').hide();
					$('div.alert-success').show();

					// set cookie
					document.cookie = 'token=' + r.data.response.data.user.token;

					// remove login form
					$('#form_login').remove();

					// redirect
					document.location = url + '/history';


				} else {
					// remove notification alert
					$('div.alert-danger').show();

					var valueConcat = '';

					$.each(r.data.response.errors, function(key, val) {
						$scope.errorData.push({ key : key, message : valueConcat.concat(val) });

						// add has-error class to specific area
						$('input[name='+ key +']').parent().parent().addClass('has-error');
					});
				}
			}

			var onError = function(r) {
				console.log({
					status : r.status,
					message : r.statusText
				});
			}
			
			$http.post(url + '/api/user/login', {
				username : $scope.username,
				password : $scope.password
			}).then(onSuccess, onError);

		}

	});

	// Register Controller
	qbanks.controller('registerController', function($scope, $http) {

		$scope.register = function() {

			$('.alert').hide();
			$('input.form-control').parent().removeClass('has-error');		

			$scope.errorData = [];

			var onSuccess = function(r) {
				// console.log(r);
				if(r.data.response.status) {
					// remove notification alert
					$('div.alert-danger').hide();
					$('div.alert-success').show();

					// remove login form
					$('#form_register').remove();

					// redirect
					document.location = url + '/login';						

				} else {
					// remove notification alert
					$('div.alert-danger').show();

					var valueConcat = '';

					$.each(r.data.response.errors, function(key, val) {
						$scope.errorData.push({ key : key, message : valueConcat.concat(val) });

						// add has-error class to specific area
						$('input[name='+ key +']').parent().addClass('has-error');
					});							

				}
			}

			var onError = function(r) {
				console.log({
					status : r.status,
					message : r.statusText
				});
			}
			
			$http.post(url + '/api/user/register', {
				firstname : $scope.firstname,
				lastname : $scope.lastname,
				username : $scope.username,
				email : $scope.email,
				password : $scope.password,
				password_confirm : $scope.confirmPassword
			}).then(onSuccess, onError);

		}

	});

	// Forgot Controller
	qbanks.controller('forgotController', function($scope, $http) {

		$scope.forgot = function() {

			$('.alert').hide();
			$('input.form-control').parent().parent().removeClass('has-error');		

			$scope.errorData = [];

			var onSuccess = function(r) {
				// console.log(r);
				if(r.data.response.status) {
					// remove notification alert
					$('div.alert-danger').hide();
					$('div.alert-success').show();

					// remove form
					$('#form_forgot').remove();

					// redirect to reset form
					document.location = url + '/reset';
					

				} else {
					// remove notification alert
					$('div.alert-danger').show();
					$('div.alert-success').hide();

					var valueConcat = '';

					$.each(r.data.response.errors, function(key, val) {
						$scope.errorData.push({ key : key, message : valueConcat.concat(val) });

						// add has-error class to specific area
						$('input[name='+ key +']').parent().parent().addClass('has-error');
					});							

				}			
			}

			var onError = function(r) {
				console.log({
					status : r.status,
					message : r.statusText
				});			
			}

			$http.post(url + '/api/user/forgot', {
				email : $scope.email,
			}).then(onSuccess, onError);

		}

	});

	// Reset Controller
	qbanks.controller('resetController', function($scope, $http) {

		$scope.reset = function() {
			$('.alert').hide();
			$('input.form-control').parent().removeClass('has-error');		

			$scope.errorData = [];

			var onSuccess = function(r) {
				// console.log(r);
				if(r.data.response.status) {
					// remove notification alert
					$('div.alert-danger').hide();
					$('div.alert-success').show();

					// remove form
					$('#form_reset').remove();

					// redirect to reset form
					document.location = url + '/login';
					

				} else {
					// remove notification alert
					$('div.alert-danger').show();
					$('div.alert-success').hide();

					var valueConcat = '';

					$.each(r.data.response.errors, function(key, val) {
						$scope.errorData.push({ key : key, message : valueConcat.concat(val) });

						// add has-error class to specific area
						$('input[name='+ key +']').parent().addClass('has-error');
					});							

				}			
			}

			var onError = function(r) {
				console.log({
					status : r.status,
					message : r.statusText
				});			
			}

			$http.get(url + '/api/user/reset_password', {
				params : {
					token : $scope.token,
					new_password : $scope.password,
					new_password_confirm : $scope.confirmPassword
				}
			}).then(onSuccess, onError);
		}

	});

	// Balance Controller
	qbanks.controller('balanceController', function($scope, $http) {

		$scope.bankLists = [];

		var getBalanceTotal = function() {

			var onSuccess = function(r) {
				// console.log(r);
				// $scope.totalBalance = r.data.response.data.total_balance;
				$('.total_balance span').text(r.data.response.data.total_balance);
			}

			var onError = function(r) {

			}

			$http.get(url + '/api/bank/balance/total', {
				headers : {
					token : getCookie('token')
				}
			}).then(onSuccess, onError);
		}	

		var getBalance = function(secretKey, bankAccountId) {

			var onSuccess = function(r) {
				// console.log(r);
				// $scope.totalBalance = r.data.response.data.total_balance;
				$('#'+ r.data.response.data.bank_account.account_number +' .active_balance span')
							.text(r.data.response.data.amount);
			}

			var onError = function(r) {

			}

			$http.get(url + '/api/bank/balance/amount', {
				headers : {
					token : getCookie('token')
				},
				params : {
					secret_key : secretKey,
					bank_account_id : bankAccountId
				}
			}).then(onSuccess, onError);
		}		

		var getBalanceHistory = function(bankAccountId) {

			var onSuccess = function(r) {
				// console.log(r);
				var balanceLists = '<table id="balance_list_'+ r.data.response.data.balance[0].bank_account.account_number 
								  +'" class="table table-striped table-bordered balance_list">';
		           	balanceLists += '<thead>';
		           	balanceLists += '<tr>';
		           	balanceLists += '<th>No</th>';
		           	balanceLists += '<th>Amount</th>';
		           	balanceLists += '<th>Update Time</th>';
		           	balanceLists += '<th>Update Date</th>';
		           	balanceLists += '</tr>';
		           	balanceLists += '</thead>';

				$.each(r.data.response.data.balance, function(key, val) {
					if(val.type == 'DB') {
						var type = 'label-danger';
					} else {
						var type = 'label-success'
					}				
					balanceLists += '<tr>';
	           		balanceLists += '<td>'+ (key+1) +'</td>';
	           		balanceLists += '<td>'+ val.amount +'</td>';
	           		balanceLists += '<td>'+ val.created_at.split(' ')[1] +'</td>';
	           		balanceLists += '<td>'+ val.created_at.split(' ')[0] +'</td>';
		            balanceLists += '</tr> ';
				});
		           	balanceLists += '<tbody></tbody>';
		           	balanceLists += '</table>';

				$('#' + r.data.response.data.balance[0].bank_account.account_number).append(balanceLists);
				createDataTable('#balance_list_' + r.data.response.data.balance[0].bank_account.account_number);			
			}

			var onError = function(r) {

			}

			$http.get(url + '/api/bank/balance/history', {
				headers : {
					token : getCookie('token')
				},
				params : {
					bank_account_id : bankAccountId
				}
			}).then(onSuccess, onError);		
		}	

		// Get bank lists
		var getBankLists = function(callback) {

			var onSuccess = function(r) {
				// console.log(r);

				// if empty data response
				if(r.data.response.status && !r.data.response.data.length) {
					document.location = url + '/register_account';
				}

				$.each(r.data.response.data, function(key, val) {
					$scope.bankLists.push({ 
						bankAccountId : val.bank_account_id,
						name : val.bank_accounts.prefix,
						accountNo : val.account_no,
						secretKey : val.secret_key,
						active : (key == 0) ? 'active' : ''
					});		

				});
				callback();
			}

			var onError = function(r) {
				console.log(r);
			}		
			$http.get(url + '/api/bank/accounts', {
				headers : {
					token : getCookie('token')
				}
			}).then(onSuccess, onError);

		}	

		$scope.getBalance = function(secretKey, bankAccountId) {
			getBalance(secretKey, bankAccountId);
		}
		$scope.getBalanceHistory = function(bankAccountId) {
			getBalanceHistory(bankAccountId);
		}
		getBankLists();
		getBalanceTotal();
	});

	// History/Dashboard Controller
	qbanks.controller('dashboardController', function($scope, $http) {

		$scope.bankLists = [];
		$scope.totalBalance = 0;
		$scope.searchByItems = [
			{ id: "bySearch", label: "Search" },
			{ id: "byUpdate", label: "Update" },
		];
		$scope.mySearchByItem = $scope.searchByItems[0];

		var setDatePicker = function(selector) {
			// alert(selector);
		    $("#date_from_" +  selector).datepicker({
		    	dateFormat: 'yy/mm/dd',
		      	minDate: "-4M",
		      	maxDate: "+0M",
		      	changeMonth: true,
		      	numberOfMonths: 1,
		      	onClose: function(selectedDate) {
		      		var newDate = new Date(selectedDate);
		      		newDate.setDate(newDate.getDate() + 29);

		      		if(newDate > new Date()) {
		      			// console.log('kegedian');
			      		// console.log(newDate.getDate());
			        	$( "#date_to_" + selector).datepicker( "option", 'minDate', selectedDate);
			        	$( "#date_to_" + selector).datepicker( "option", 'maxDate', '0');		      			
		      		} else {
			      		// console.log(newDate.getDate());
			        	$( "#date_to_" + selector).datepicker( "option", 'minDate', selectedDate);
			        	$( "#date_to_" + selector).datepicker( "option", 'maxDate', newDate);
		      		}
		      	}
		    });		
		    $( "#date_to_" + selector).datepicker({
		    	dateFormat: 'yy/mm/dd',
		    	changeMonth: true,
		    });		    
		}

		var updateBalance = function(secretKey) {

			var onSuccess = function(r) {
				// console.log(r);
				// $scope.totalBalance = r.data.response.data
				// setDatePicker(r.data.response.data.account_number);
			}

			var onError = function(r) {

			}

			$http.get(url + '/api/bank/balance', {
				params : {
					secret_key : secretKey
				},
				headers : {
					token : getCookie('token')
				}
			}).then(onSuccess, onError);
		}	

		var getBalance = function() {

			var onSuccess = function(r) {
				// console.log(r);
				// $scope.totalBalance = r.data.response.data.total_balance;
				$('.total_balance span').text(r.data.response.data.total_balance);
				unloading('body');
			}

			var onError = function(r) {

			}

			$http.get(url + '/api/bank/balance/total', {
				headers : {
					token : getCookie('token')
				}
			}).then(onSuccess, onError);
		}

		// Find old cashflow form database
		var oldCashflow = function(accountId) {

			var onSuccess = function (r) {
				// console.log(r);

				var cashflowLists = '<table id="cashflow_list_'+ r.data.response.account_number 
								  +'" class="table table-striped table-bordered cashflow_list">';
		           	cashflowLists += '<thead>';
		           	cashflowLists += '<tr>';
		           	cashflowLists += '<th>Type</th>';
		           	cashflowLists += '<th>Description</th>';
		           	cashflowLists += '<th>Date</th>';
		           	cashflowLists += '<th>Amount</th>';
		           	cashflowLists += '</tr>';
		           	cashflowLists += '</thead>';

				$.each(r.data.response.data, function(key, val) {
					if(val.type == 'DB') {
						var type = 'label-danger';
					} else {
						var type = 'label-success'
					}				
					cashflowLists += '<tr>';
	           		cashflowLists += '<td><span class="label '+ type +'">'+ val.type +'</span></td>';
	           		cashflowLists += '<td>'+ val.description +'</td>';
	           		cashflowLists += '<td>'+ val.date +'</td>';
	           		cashflowLists += '<td>'+ val.amount +'</td>';
		            cashflowLists += '</tr> ';
				});
		           	cashflowLists += '<tbody></tbody>';
		           	cashflowLists += '</table>';

		        $('#cashflow_list_' + r.data.response.account_number + '_wrapper').remove();
				$('#' + r.data.response.account_number + ' .list-group').append(cashflowLists);
				createDataTable('#cashflow_list_' + r.data.response.account_number);

				// set datepicker
				setDatePicker(r.data.response.account_number);
			}

			var onError = function(r) {

			}

			$http.get(url + '/api/bank/transaction/last_history', {
				headers : {
					token : getCookie('token')
				},
				params : {
					sort_type : 'date',
					order_by : 'desc',
					bank_account_id : accountId,
					limit: 50
				}
			}).then(onSuccess, onError);
		}

		// Find new cashflow by scraping
		var newCashflow = function(from, to, secretKey, accountId) {

			var onSuccess = function(r) {
				console.log(r);
				$scope.errorData = {};

				if(r.data.response.status) {
					// remove notification alert
					$('div.alert-danger').hide();
				} else {
					// remove notification alert
					$('div.alert-danger').show();
					var valueConcat = '';
					$.each(r.data.response.errors, function(key, val) {
						$scope.errorData = { key : key, message : valueConcat.concat(val) };
					});
				}

				var cashflowLists = '<table id="cashflow_list_'+ r.data.response.data.account_number 
								  +'" class="table table-striped table-bordered cashflow_list">';
		           	cashflowLists += '<thead>';
		           	cashflowLists += '<tr>';
		           	cashflowLists += '<th>Type</th>';
		           	cashflowLists += '<th>Description</th>';
		           	cashflowLists += '<th>Date</th>';
		           	cashflowLists += '<th>Amount</th>';
		           	cashflowLists += '</tr>';
		           	cashflowLists += '</thead>';

				$.each(r.data.response.data.history, function(key, val) {
					if(val.type == 'DB') {
						var type = 'label-danger';
					} else {
						var type = 'label-success'
					}				
					cashflowLists += '<tr>';
	           		cashflowLists += '<td><span class="label '+ type +'">'+ val.type +'</span></td>';
	           		cashflowLists += '<td>'+ val.desc +'</td>';
	           		cashflowLists += '<td>'+ val.date +'</td>';
	           		cashflowLists += '<td>'+ parseInt(val.amount) +'</td>';
		            cashflowLists += '</tr> ';
				});

	           	cashflowLists += '<tbody></tbody>';
	           	cashflowLists += '</table>';

				$('#cashflow_list_' + r.data.response.data.account_number + '_wrapper').remove();
				$('#cashflow_list_' + r.data.response.data.account_number).remove();
				$('#' + r.data.response.data.account_number + ' .list-group').append(cashflowLists);		
				createDataTable("#cashflow_list_" + r.data.response.data.account_number);           	

				// $('#cashflow_list_' + r.data.response.data.account_number + '_wrapper').remove();
				// createDataTable("#cashflow_list_" + r.data.response.data.account_number);

				unloading('body');
			}

			var onError = function(r) {
				unloading('body');
				console.log(r);
			}

			$http.get(url + '/api/bank/transaction/history', {
				headers : {
					token : getCookie('token')
				},
				params : {
					secret_key : secretKey,
					from : from,
					to : to,
					sort_type : 'date',
					order_by : 'desc',
					bank_account_id : accountId,
				}
			}).then(onSuccess, onError);		
		}

		// Get bank lists
		var getBankLists = function(callback) {

			var onSuccess = function(r) {
				// console.log(r);
				
				// if empty response data
				if(r.data.response.status && !r.data.response.data.length) {
					document.location = url + '/register_account';
				}

				$.each(r.data.response.data, function(key, val) {
					$scope.bankLists.push({ 
						bankAccountId : val.bank_account_id,
						name : val.bank_accounts.prefix,
						accountNo : val.account_no,
						secretKey : val.secret_key,
						active : (key == 0) ? 'active' : ''
					});		

					oldCashflow(val.bank_account_id)
					loading('body');
					updateBalance(val.secret_key);

				});
				callback();
			}

			var onError = function(r) {
				console.log(r);
			}		
			$http.get(url + '/api/bank/accounts', {
				headers : {
					token : getCookie('token')
				}
			}).then(onSuccess, onError);

		}

		// Search cashflow by date
		var searchCashflow = function(from, to, secretKey, bankAccountId) {

			var onError = function(r) {
				console.log(r);
			}

			var onSuccess = function(r) {
				console.log(r);
				$scope.errorData = {};

				if(r.data.response.status) {
					// remove notification alert
					$('div.alert-danger').hide();
				} else {
					// remove notification alert
					$('div.alert-danger').show();
					var valueConcat = '';
					$.each(r.data.response.errors, function(key, val) {
						$scope.errorData = { key : key, message : valueConcat.concat(val) };
					});
				}

				var cashflowLists = '<table id="cashflow_list_'+ r.data.response.data.account_number 
								  +'" class="table table-striped table-bordered cashflow_list">';
		           	cashflowLists += '<thead>';
		           	cashflowLists += '<tr>';
		           	cashflowLists += '<th>Type</th>';
		           	cashflowLists += '<th>Description</th>';
		           	cashflowLists += '<th>Date</th>';
		           	cashflowLists += '<th>Amount</th>';
		           	cashflowLists += '</tr>';
		           	cashflowLists += '</thead>';

				$.each(r.data.response.data.history, function(key, val) {
					if(val.type == 'DB') {
						var type = 'label-danger';
					} else {
						var type = 'label-success'
					}				
					cashflowLists += '<tr>';
	           		cashflowLists += '<td><span class="label '+ type +'">'+ val.type +'</span></td>';
	           		cashflowLists += '<td>'+ val.description +'</td>';
	           		cashflowLists += '<td>'+ val.date +'</td>';
	           		cashflowLists += '<td>'+ parseInt(val.amount) +'</td>';
		            cashflowLists += '</tr> ';
				});

	           	cashflowLists += '<tbody></tbody>';
	           	cashflowLists += '</table>';

				$('#cashflow_list_' + r.data.response.data.account_number + '_wrapper').remove();
				$('#cashflow_list_' + r.data.response.data.account_number).remove();
				$('#' + r.data.response.data.account_number + ' .list-group').append(cashflowLists);		
				createDataTable("#cashflow_list_" + r.data.response.data.account_number);

				unloading('body');			

			}

			$http.get(url + '/api/bank/cashflow/search', {
				headers : {
					token : getCookie('token')
				},
				params : {
					from : from,
					to : to,
					bank_account_id : bankAccountId,
				}
			}).then(onSuccess, onError);			
		}

		getBankLists(function() {
			getBalance();
		});

		// Event search button
		$scope.search = function(from, to, secretKey, bankAccountId, mySearchByItem) {
			loading('body');

			if(mySearchByItem == "byUpdate") {
				newCashflow(from, to, secretKey, bankAccountId);
			} else if(mySearchByItem == "bySearch") {
				searchCashflow(from, to, secretKey, bankAccountId);
			}
		}	

		$scope.refresh = function() {
			document.location = url + '/dashboard';
		}	

	});

	// Register Bank Controller
	qbanks.controller('registerBankController', function($scope, $http) {

		$scope.bankLists = [];
		$scope.bank = [];

		var getBalanceTotal = function() {

			var onSuccess = function(r) {
				// console.log(r);
				// $scope.totalBalance = r.data.response.data.total_balance;
				$('.total_balance span').text(r.data.response.data.total_balance);
			}

			var onError = function(r) {

			}

			$http.get(url + '/api/bank/balance/total', {
				headers : {
					token : getCookie('token')
				}
			}).then(onSuccess, onError);
		}	

		// Get bank lists
		var getBankLists = function() {

			var onSuccess = function(r) {
				// console.log(r);
				$.each(r.data.response.data.bank, function(key, val) {
					$scope.bankLists.push(val);		

				});
				$scope.bank = $scope.bankLists[0];
			}

			var onError = function(r) {
				console.log(r);
			}		
			$http.get(url + '/api/bank', {
				headers : {
					token : getCookie('token')
				}
			}).then(onSuccess, onError);

		}

		$scope.registerAccount = function(bankId, username, password, accountNo) {

			// Menampilkan loading
			loading("body");

			$('.alert').hide();
			$('input.form-control').parent().removeClass('has-error');		

			$scope.errorData = [];		

			var onSuccess = function(r) {
				console.log(r)
				unloading("body");

				if(r.data.response.status) {
					// remove notification alert
					$('div.alert-danger').hide();
					$('div.alert-success').show();

					// redirect
					document.location = url + '/register_account';


				} else {
					// remove notification alert
					$('div.alert-danger').show();

					var valueConcat = '';

					$.each(r.data.response.errors, function(key, val) {
						$scope.errorData.push({ key : key, message : valueConcat.concat(val) });

						// add has-error class to specific area
						$('input[name='+ key +']').parent().addClass('has-error');
					});
				}			
			}

			var onError = function(r) {
				console.log('Error: ', r.data);
			}

			$http.post(url + '/api/bank/register', {
				bank_id : bankId,
				username : username,
				password : password,
				account_number : accountNo
			}, {
				headers : {
					token : getCookie('token')
				}
			}).then(onSuccess, onError);		
		}

		getBankLists();
		getBalanceTotal();

	});

	// Logout Controller
	qbanks.controller('logoutController', function($scope, $http) {

		$http.get(url + '/api/user/logout', {
			headers : {
				token : getCookie('token')
			}
		}).then(function(r) {
			// console.log(r);
			document.location = url;

		}, function(r) {
			console.log('Error: ', r);
		});	

	});

	// Authorize token Controller
	qbanks.controller('authorizeController', function($scope, $http) {

		$http.post(url + '/api/user/check_token', {}, {
			headers : {
				token : getCookie('token')
			}
		}).then(function(r) {
			// console.log(r);
			if( ! r.data.response.status) {
				document.location = url;
			}

		}, function(r) {
			console.log('Error: ', r);
		});	

	});

	// Change Password Controller
	qbanks.controller('changePasswordController', function($scope, $http) {

		var getBalanceTotal = function() {

			var onSuccess = function(r) {
				// console.log(r);
				// $scope.totalBalance = r.data.response.data.total_balance;
				$('.total_balance span').text(r.data.response.data.total_balance);
			}

			var onError = function(r) {

			}

			$http.get(url + '/api/bank/balance/total', {
				headers : {
					token : getCookie('token')
				}
			}).then(onSuccess, onError);
		}	

		$scope.changePassword = function(password, new_password, confirm_new_password) {

			$('.alert').hide();
			$('input.form-control').parent().removeClass('has-error');		
			$scope.errorData = [];

			var onSuccess = function(r) {
				// console.log(r);
				if(r.data.response.status) {
					// remove notification alert
					$('div.alert-danger').hide();
					$('div.alert-success').show();

					// redirect
					document.location = url + '/login';


				} else {
					// remove notification alert
					$('div.alert-danger').show();

					var valueConcat = '';

					$.each(r.data.response.errors, function(key, val) {
						$scope.errorData.push({ key : key, message : valueConcat.concat(val) });

						// add has-error class to specific area
						$('input[name='+ key +']').parent().addClass('has-error');
					});
				}
			}

			var onError = function(r) {
				console.log('Error: ', r);
			}

			$http.post(url + '/api/user/change_password', {
				password : password,
				new_password : new_password,
				new_password_confirm: confirm_new_password
			}, 
			{
				headers : {
					token : getCookie('token')
			}
			}).then(onSuccess, onError);			
		}

		getBalanceTotal();
		
	});

	// Remove account Controller
	qbanks.controller('removeAccountController', function($scope, $http) {

		var getBalanceTotal = function() {

			var onSuccess = function(r) {
				// console.log(r);
				// $scope.totalBalance = r.data.response.data.total_balance;
				$('.total_balance span').text(r.data.response.data.total_balance);
			}

			var onError = function(r) {

			}

			$http.get(url + '/api/bank/balance/total', {
				headers : {
					token : getCookie('token')
				}
			}).then(onSuccess, onError);
		}	

		$scope.delete = function(id) {

			$('.alert').hide();
			$('input.form-control').parent().removeClass('has-error');		
			$scope.errorData = [];		

			var onSuccess = function(r) {
				if(r.data.response.status) {
					// remove notification alert
					$('div.alert-danger').hide();
					$('div.alert-success').show();

					// redirect
					document.location = url + '/settings/bank_account_lists';


				} else {
					// remove notification alert
					$('div.alert-danger').show();

					var valueConcat = '';

					$.each(r.data.response.errors, function(key, val) {
						$scope.errorData.push({ key : key, message : valueConcat.concat(val) });

						// add has-error class to specific area
						$('input[name='+ key +']').parent().addClass('has-error');
					});
				}			
			}

			$http.get(url + '/api/bank/account/delete', {
				headers : {
					token : getCookie('token')
				},
				params : {
					bank_account_id : id
				}
			}).then(onSuccess,
			function(r) {
				console.log('Error: ', r);
			});	
		}

		// Get bank lists
		var getBankLists = function(callback) {

			var onSuccess = function(r) {
				$scope.accounts = r.data.response.data;
			}

			var onError = function(r) {
				console.log(r);
			}		
			$http.get(url + '/api/bank/accounts', {
				headers : {
					token : getCookie('token')
				}
			}).then(onSuccess, onError);
		}	

		getBalanceTotal();
		getBankLists();

	});

})();