<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditBankAccountSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_account_settings', function ($table) {
            $table->dropColumn('settings');
        });

        Schema::table('bank_account_settings', function ($table) {
            $table->text('settings');
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_account_settings', function ($table) {
            //
        });
    }
}
