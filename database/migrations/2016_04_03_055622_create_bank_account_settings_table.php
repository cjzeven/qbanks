<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_account_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_account_id')->unsigned();
            $table->string('settings');
            $table->timestamps();
        });

        Schema::table('bank_account_settings', function($table) {
            $table->foreign('bank_account_id')
                  ->references('id')
                  ->on('bank_accounts')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');             
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bank_account_settings');
    }
}
