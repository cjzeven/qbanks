<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashflows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_account_id')->unsigned();
            $table->string('description');
            $table->string('user_description');
            $table->date('date');
            $table->double('amount');
            $table->string('type');
            $table->enum('status', ['unread', 'read']);
            $table->timestamps();
        });

        Schema::table('cashflows', function($table) {
            $table->foreign('bank_account_id')
                  ->references('id')
                  ->on('bank_accounts')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cashflows');
    }
}
